
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:20PM

See merge request itentialopensource/adapters/adapter-juniper_northstar!12

---

## 0.4.3 [08-30-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-juniper_northstar!10

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_18:31PM

See merge request itentialopensource/adapters/adapter-juniper_northstar!9

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_19:44PM

See merge request itentialopensource/adapters/adapter-juniper_northstar!8

---

## 0.4.0 [07-08-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/controller-orchestrator/adapter-juniper_northstar!7

---

## 0.3.4 [03-28-2024]

* Changes made at 2024.03.28_13:38PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-juniper_northstar!6

---

## 0.3.3 [03-13-2024]

* Changes made at 2024.03.13_12:44PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-juniper_northstar!5

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_15:56PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-juniper_northstar!4

---

## 0.3.1 [02-28-2024]

* Changes made at 2024.02.28_11:24AM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-juniper_northstar!3

---

## 0.3.0 [12-28-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/controller-orchestrator/adapter-juniper_northstar!2

---

## 0.2.0 [05-21-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-juniper_northstar!1

---

## 0.1.1 [10-05-2021]

- Initial Commit

See commit 2313d3e

---
