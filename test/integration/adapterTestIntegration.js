/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-juniper_northstar',
      type: 'JuniperNorthstar',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const JuniperNorthstar = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Juniper-northstar Adapter Test', () => {
  describe('JuniperNorthstar Class Tests', () => {
    const a = new JuniperNorthstar(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-juniper_northstar-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-juniper_northstar-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const tenantTenantId = 'fakedata';
    const tenantPostV2TenantTenantIdHighAvailabilityStepdownBodyParam = [
      {
        preferenceType: 'string',
        host: 'string',
        priority: 6
      }
    ];
    describe('#postV2TenantTenantIdHighAvailabilityStepdown - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdHighAvailabilityStepdown(tenantTenantId, tenantPostV2TenantTenantIdHighAvailabilityStepdownBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdHighAvailabilityStepdown', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPostV2TenantTenantIdNetconfNetconfCollectionLiveNetworkBodyParam = [
      {
        demandIndex: 3
      }
    ];
    describe('#postV2TenantTenantIdNetconfNetconfCollectionLiveNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdNetconfNetconfCollectionLiveNetwork(tenantTenantId, tenantPostV2TenantTenantIdNetconfNetconfCollectionLiveNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdNetconfNetconfCollectionLiveNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPostV2TenantTenantIdStatisticsChildtrafficBulkBodyParam = [
      {
        topologyIndex: 9,
        topologyObjectType: 'string',
        node: {
          topoObjectType: 'string',
          nodeIndex: 4,
          id: 'string',
          topologyIndex: 5
        },
        table: 'string',
        prefix: {
          topoObjectType: 'string',
          address: 'string',
          length: 8
        },
        pathCookie: 2,
        asPath: [
          'string',
          'string'
        ],
        protocolNextHops: [
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocol: 'string',
        routePreference: 1,
        localPreference: 7,
        vpnLabel: 3
      }
    ];
    describe('#postV2TenantTenantIdStatisticsChildtrafficBulk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsChildtrafficBulk(tenantTenantId, tenantPostV2TenantTenantIdStatisticsChildtrafficBulkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdStatisticsChildtrafficBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPostV2TenantTenantIdStatisticsDelayFieldsBodyParam = [
      {
        topologyIndex: 8,
        topologyObjectType: 'string',
        node: {
          topoObjectType: 'string',
          nodeIndex: 9,
          id: 'string',
          topologyIndex: 6
        },
        table: 'string',
        prefix: {
          topoObjectType: 'string',
          address: 'string',
          length: 5
        },
        pathCookie: 10,
        asPath: [
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocolNextHops: [
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocol: 'string',
        routePreference: 5,
        localPreference: 4,
        vpnLabel: 4
      }
    ];
    describe('#postV2TenantTenantIdStatisticsDelayFields - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsDelayFields(tenantTenantId, tenantPostV2TenantTenantIdStatisticsDelayFieldsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdStatisticsDelayFields', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPostV2TenantTenantIdStatisticsDemandsBulkBodyParam = [
      {
        topologyIndex: 1,
        topologyObjectType: 'string',
        node: {
          topoObjectType: 'string',
          nodeIndex: 8,
          id: 'string',
          topologyIndex: 8
        },
        table: 'string',
        prefix: {
          topoObjectType: 'string',
          address: 'string',
          length: 8
        },
        pathCookie: 5,
        asPath: [
          'string',
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocolNextHops: [
          'string',
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocol: 'string',
        routePreference: 4,
        localPreference: 4,
        vpnLabel: 7
      }
    ];
    describe('#postV2TenantTenantIdStatisticsDemandsBulk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsDemandsBulk(tenantTenantId, tenantPostV2TenantTenantIdStatisticsDemandsBulkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdStatisticsDemandsBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPostV2TenantTenantIdStatisticsDeviceBulkBodyParam = [
      {
        topologyIndex: 3,
        topologyObjectType: 'string',
        node: {
          topoObjectType: 'string',
          nodeIndex: 8,
          id: 'string',
          topologyIndex: 3
        },
        table: 'string',
        prefix: {
          topoObjectType: 'string',
          address: 'string',
          length: 7
        },
        pathCookie: 5,
        asPath: [
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocolNextHops: [
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocol: 'string',
        routePreference: 9,
        localPreference: 4,
        vpnLabel: 3
      }
    ];
    describe('#postV2TenantTenantIdStatisticsDeviceBulk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsDeviceBulk(tenantTenantId, tenantPostV2TenantTenantIdStatisticsDeviceBulkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdStatisticsDeviceBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPostV2TenantTenantIdStatisticsDeviceTopBodyParam = [
      {
        topologyIndex: 2,
        topologyObjectType: 'string',
        node: {
          topoObjectType: 'string',
          nodeIndex: 10,
          id: 'string',
          topologyIndex: 4
        },
        table: 'string',
        prefix: {
          topoObjectType: 'string',
          address: 'string',
          length: 8
        },
        pathCookie: 10,
        asPath: [
          'string',
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocolNextHops: [
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocol: 'string',
        routePreference: 8,
        localPreference: 9,
        vpnLabel: 2
      }
    ];
    describe('#postV2TenantTenantIdStatisticsDeviceTop - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsDeviceTop(tenantTenantId, tenantPostV2TenantTenantIdStatisticsDeviceTopBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdStatisticsDeviceTop', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPostV2TenantTenantIdStatisticsInterfacesBulkBodyParam = [
      {
        topologyIndex: 5,
        topologyObjectType: 'string',
        node: {
          topoObjectType: 'string',
          nodeIndex: 6,
          id: 'string',
          topologyIndex: 5
        },
        table: 'string',
        prefix: {
          topoObjectType: 'string',
          address: 'string',
          length: 1
        },
        pathCookie: 1,
        asPath: [
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocolNextHops: [
          'string'
        ],
        protocol: 'string',
        routePreference: 6,
        localPreference: 7,
        vpnLabel: 5
      }
    ];
    describe('#postV2TenantTenantIdStatisticsInterfacesBulk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsInterfacesBulk(tenantTenantId, tenantPostV2TenantTenantIdStatisticsInterfacesBulkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdStatisticsInterfacesBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPostV2TenantTenantIdStatisticsInterfacesBulkdelayBodyParam = [
      {
        topologyIndex: 6,
        topologyObjectType: 'string',
        node: {
          topoObjectType: 'string',
          nodeIndex: 6,
          id: 'string',
          topologyIndex: 2
        },
        table: 'string',
        prefix: {
          topoObjectType: 'string',
          address: 'string',
          length: 7
        },
        pathCookie: 1,
        asPath: [
          'string',
          'string'
        ],
        protocolNextHops: [
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocol: 'string',
        routePreference: 6,
        localPreference: 7,
        vpnLabel: 10
      }
    ];
    describe('#postV2TenantTenantIdStatisticsInterfacesBulkdelay - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsInterfacesBulkdelay(tenantTenantId, tenantPostV2TenantTenantIdStatisticsInterfacesBulkdelayBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdStatisticsInterfacesBulkdelay', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantDeviceName = 'fakedata';
    const tenantInterfaceName = 'fakedata';
    const tenantPostV2TenantTenantIdStatisticsInterfacesChildtrafficDeviceNameInterfaceNameBodyParam = [
      {
        topologyIndex: 4,
        topologyObjectType: 'string',
        node: {
          topoObjectType: 'string',
          nodeIndex: 9,
          id: 'string',
          topologyIndex: 3
        },
        table: 'string',
        prefix: {
          topoObjectType: 'string',
          address: 'string',
          length: 10
        },
        pathCookie: 9,
        asPath: [
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocolNextHops: [
          'string'
        ],
        protocol: 'string',
        routePreference: 4,
        localPreference: 6,
        vpnLabel: 1
      }
    ];
    describe('#postV2TenantTenantIdStatisticsInterfacesChildtrafficDeviceNameInterfaceName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsInterfacesChildtrafficDeviceNameInterfaceName(tenantTenantId, tenantDeviceName, tenantInterfaceName, tenantPostV2TenantTenantIdStatisticsInterfacesChildtrafficDeviceNameInterfaceNameBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdStatisticsInterfacesChildtrafficDeviceNameInterfaceName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPostV2TenantTenantIdStatisticsInterfacesDelayDeviceNameInterfaceNameBodyParam = [
      {
        topologyIndex: 4,
        topologyObjectType: 'string',
        node: {
          topoObjectType: 'string',
          nodeIndex: 1,
          id: 'string',
          topologyIndex: 6
        },
        table: 'string',
        prefix: {
          topoObjectType: 'string',
          address: 'string',
          length: 10
        },
        pathCookie: 9,
        asPath: [
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocolNextHops: [
          'string',
          'string',
          'string'
        ],
        protocol: 'string',
        routePreference: 10,
        localPreference: 4,
        vpnLabel: 4
      }
    ];
    describe('#postV2TenantTenantIdStatisticsInterfacesDelayDeviceNameInterfaceName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsInterfacesDelayDeviceNameInterfaceName(tenantTenantId, tenantDeviceName, tenantInterfaceName, tenantPostV2TenantTenantIdStatisticsInterfacesDelayDeviceNameInterfaceNameBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdStatisticsInterfacesDelayDeviceNameInterfaceName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPostV2TenantTenantIdStatisticsInterfacesFieldsBodyParam = [
      {
        topologyIndex: 3,
        topologyObjectType: 'string',
        node: {
          topoObjectType: 'string',
          nodeIndex: 4,
          id: 'string',
          topologyIndex: 10
        },
        table: 'string',
        prefix: {
          topoObjectType: 'string',
          address: 'string',
          length: 7
        },
        pathCookie: 2,
        asPath: [
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocolNextHops: [
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocol: 'string',
        routePreference: 2,
        localPreference: 1,
        vpnLabel: 9
      }
    ];
    describe('#postV2TenantTenantIdStatisticsInterfacesFields - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsInterfacesFields(tenantTenantId, tenantPostV2TenantTenantIdStatisticsInterfacesFieldsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdStatisticsInterfacesFields', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPostV2TenantTenantIdStatisticsInterfacesTopBodyParam = [
      {
        topologyIndex: 3,
        topologyObjectType: 'string',
        node: {
          topoObjectType: 'string',
          nodeIndex: 8,
          id: 'string',
          topologyIndex: 10
        },
        table: 'string',
        prefix: {
          topoObjectType: 'string',
          address: 'string',
          length: 3
        },
        pathCookie: 5,
        asPath: [
          'string'
        ],
        protocolNextHops: [
          'string',
          'string',
          'string',
          'string'
        ],
        protocol: 'string',
        routePreference: 8,
        localPreference: 5,
        vpnLabel: 2
      }
    ];
    describe('#postV2TenantTenantIdStatisticsInterfacesTop - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsInterfacesTop(tenantTenantId, tenantPostV2TenantTenantIdStatisticsInterfacesTopBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdStatisticsInterfacesTop', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPostV2TenantTenantIdStatisticsInterfacesTopdelayBodyParam = [
      {
        topologyIndex: 2,
        topologyObjectType: 'string',
        node: {
          topoObjectType: 'string',
          nodeIndex: 5,
          id: 'string',
          topologyIndex: 10
        },
        table: 'string',
        prefix: {
          topoObjectType: 'string',
          address: 'string',
          length: 4
        },
        pathCookie: 5,
        asPath: [
          'string',
          'string',
          'string',
          'string'
        ],
        protocolNextHops: [
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocol: 'string',
        routePreference: 5,
        localPreference: 8,
        vpnLabel: 8
      }
    ];
    describe('#postV2TenantTenantIdStatisticsInterfacesTopdelay - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsInterfacesTopdelay(tenantTenantId, tenantPostV2TenantTenantIdStatisticsInterfacesTopdelayBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdStatisticsInterfacesTopdelay', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPostV2TenantTenantIdStatisticsInterfacesTrafficBodyParam = [
      {
        topologyIndex: 6,
        topologyObjectType: 'string',
        node: {
          topoObjectType: 'string',
          nodeIndex: 7,
          id: 'string',
          topologyIndex: 3
        },
        table: 'string',
        prefix: {
          topoObjectType: 'string',
          address: 'string',
          length: 1
        },
        pathCookie: 10,
        asPath: [
          'string'
        ],
        protocolNextHops: [
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocol: 'string',
        routePreference: 1,
        localPreference: 6,
        vpnLabel: 2
      }
    ];
    describe('#postV2TenantTenantIdStatisticsInterfacesTraffic - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsInterfacesTraffic(tenantTenantId, tenantPostV2TenantTenantIdStatisticsInterfacesTrafficBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdStatisticsInterfacesTraffic', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPostV2TenantTenantIdStatisticsInterfacesTrafficDeviceNameBodyParam = [
      {
        topologyIndex: 1,
        topologyObjectType: 'string',
        node: {
          topoObjectType: 'string',
          nodeIndex: 6,
          id: 'string',
          topologyIndex: 9
        },
        table: 'string',
        prefix: {
          topoObjectType: 'string',
          address: 'string',
          length: 10
        },
        pathCookie: 1,
        asPath: [
          'string',
          'string',
          'string',
          'string'
        ],
        protocolNextHops: [
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocol: 'string',
        routePreference: 10,
        localPreference: 8,
        vpnLabel: 9
      }
    ];
    describe('#postV2TenantTenantIdStatisticsInterfacesTrafficDeviceName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsInterfacesTrafficDeviceName(tenantTenantId, tenantDeviceName, tenantPostV2TenantTenantIdStatisticsInterfacesTrafficDeviceNameBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdStatisticsInterfacesTrafficDeviceName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPostV2TenantTenantIdStatisticsInterfacesTrafficDeviceNameInterfaceNameBodyParam = [
      {
        topologyIndex: 1,
        topologyObjectType: 'string',
        node: {
          topoObjectType: 'string',
          nodeIndex: 6,
          id: 'string',
          topologyIndex: 8
        },
        table: 'string',
        prefix: {
          topoObjectType: 'string',
          address: 'string',
          length: 9
        },
        pathCookie: 3,
        asPath: [
          'string',
          'string',
          'string'
        ],
        protocolNextHops: [
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocol: 'string',
        routePreference: 4,
        localPreference: 6,
        vpnLabel: 10
      }
    ];
    describe('#postV2TenantTenantIdStatisticsInterfacesTrafficDeviceNameInterfaceName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsInterfacesTrafficDeviceNameInterfaceName(tenantTenantId, tenantDeviceName, tenantInterfaceName, tenantPostV2TenantTenantIdStatisticsInterfacesTrafficDeviceNameInterfaceNameBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdStatisticsInterfacesTrafficDeviceNameInterfaceName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPostV2TenantTenantIdStatisticsJnxcosBulkBodyParam = [
      {
        topologyIndex: 4,
        topologyObjectType: 'string',
        node: {
          topoObjectType: 'string',
          nodeIndex: 10,
          id: 'string',
          topologyIndex: 2
        },
        table: 'string',
        prefix: {
          topoObjectType: 'string',
          address: 'string',
          length: 4
        },
        pathCookie: 7,
        asPath: [
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocolNextHops: [
          'string'
        ],
        protocol: 'string',
        routePreference: 9,
        localPreference: 5,
        vpnLabel: 10
      }
    ];
    describe('#postV2TenantTenantIdStatisticsJnxcosBulk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsJnxcosBulk(tenantTenantId, tenantPostV2TenantTenantIdStatisticsJnxcosBulkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdStatisticsJnxcosBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPostV2TenantTenantIdStatisticsLdpLspsBulkBodyParam = [
      {
        topologyIndex: 2,
        topologyObjectType: 'string',
        node: {
          topoObjectType: 'string',
          nodeIndex: 6,
          id: 'string',
          topologyIndex: 4
        },
        table: 'string',
        prefix: {
          topoObjectType: 'string',
          address: 'string',
          length: 8
        },
        pathCookie: 1,
        asPath: [
          'string',
          'string',
          'string',
          'string'
        ],
        protocolNextHops: [
          'string',
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocol: 'string',
        routePreference: 3,
        localPreference: 5,
        vpnLabel: 7
      }
    ];
    describe('#postV2TenantTenantIdStatisticsLdpLspsBulk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsLdpLspsBulk(tenantTenantId, tenantPostV2TenantTenantIdStatisticsLdpLspsBulkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdStatisticsLdpLspsBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantFec = 'fakedata';
    const tenantPostV2TenantTenantIdStatisticsLdpLspsTrafficDeviceNameFecBodyParam = [
      {
        topologyIndex: 10,
        topologyObjectType: 'string',
        node: {
          topoObjectType: 'string',
          nodeIndex: 9,
          id: 'string',
          topologyIndex: 2
        },
        table: 'string',
        prefix: {
          topoObjectType: 'string',
          address: 'string',
          length: 2
        },
        pathCookie: 8,
        asPath: [
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocolNextHops: [
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocol: 'string',
        routePreference: 7,
        localPreference: 2,
        vpnLabel: 3
      }
    ];
    describe('#postV2TenantTenantIdStatisticsLdpLspsTrafficDeviceNameFec - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsLdpLspsTrafficDeviceNameFec(tenantTenantId, tenantDeviceName, tenantFec, tenantPostV2TenantTenantIdStatisticsLdpLspsTrafficDeviceNameFecBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdStatisticsLdpLspsTrafficDeviceNameFec', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPostV2TenantTenantIdStatisticsNetflowPrefixFlowDemandBodyParam = [
      {
        topologyIndex: 6,
        topologyObjectType: 'string',
        node: {
          topoObjectType: 'string',
          nodeIndex: 7,
          id: 'string',
          topologyIndex: 6
        },
        table: 'string',
        prefix: {
          topoObjectType: 'string',
          address: 'string',
          length: 5
        },
        pathCookie: 10,
        asPath: [
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocolNextHops: [
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocol: 'string',
        routePreference: 8,
        localPreference: 2,
        vpnLabel: 1
      }
    ];
    describe('#postV2TenantTenantIdStatisticsNetflowPrefixFlowDemand - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsNetflowPrefixFlowDemand(tenantTenantId, tenantPostV2TenantTenantIdStatisticsNetflowPrefixFlowDemandBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdStatisticsNetflowPrefixFlowDemand', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPostV2TenantTenantIdStatisticsSidBulkBodyParam = [
      {
        topologyIndex: 1,
        topologyObjectType: 'string',
        node: {
          topoObjectType: 'string',
          nodeIndex: 8,
          id: 'string',
          topologyIndex: 2
        },
        table: 'string',
        prefix: {
          topoObjectType: 'string',
          address: 'string',
          length: 10
        },
        pathCookie: 3,
        asPath: [
          'string',
          'string',
          'string'
        ],
        protocolNextHops: [
          'string',
          'string'
        ],
        protocol: 'string',
        routePreference: 2,
        localPreference: 5,
        vpnLabel: 5
      }
    ];
    describe('#postV2TenantTenantIdStatisticsSidBulk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsSidBulk(tenantTenantId, tenantPostV2TenantTenantIdStatisticsSidBulkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdStatisticsSidBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPostV2TenantTenantIdStatisticsSrTePolicyBulkBodyParam = [
      {
        topologyIndex: 8,
        topologyObjectType: 'string',
        node: {
          topoObjectType: 'string',
          nodeIndex: 4,
          id: 'string',
          topologyIndex: 3
        },
        table: 'string',
        prefix: {
          topoObjectType: 'string',
          address: 'string',
          length: 5
        },
        pathCookie: 4,
        asPath: [
          'string',
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocolNextHops: [
          'string'
        ],
        protocol: 'string',
        routePreference: 8,
        localPreference: 7,
        vpnLabel: 2
      }
    ];
    describe('#postV2TenantTenantIdStatisticsSrTePolicyBulk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsSrTePolicyBulk(tenantTenantId, tenantPostV2TenantTenantIdStatisticsSrTePolicyBulkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdStatisticsSrTePolicyBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPostV2TenantTenantIdStatisticsTeLspsBulkBodyParam = [
      {
        topologyIndex: 6,
        topologyObjectType: 'string',
        node: {
          topoObjectType: 'string',
          nodeIndex: 1,
          id: 'string',
          topologyIndex: 9
        },
        table: 'string',
        prefix: {
          topoObjectType: 'string',
          address: 'string',
          length: 5
        },
        pathCookie: 5,
        asPath: [
          'string',
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocolNextHops: [
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocol: 'string',
        routePreference: 2,
        localPreference: 10,
        vpnLabel: 9
      }
    ];
    describe('#postV2TenantTenantIdStatisticsTeLspsBulk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsTeLspsBulk(tenantTenantId, tenantPostV2TenantTenantIdStatisticsTeLspsBulkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdStatisticsTeLspsBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPostV2TenantTenantIdStatisticsTeLspsBulkdelayBodyParam = [
      {
        topologyIndex: 7,
        topologyObjectType: 'string',
        node: {
          topoObjectType: 'string',
          nodeIndex: 6,
          id: 'string',
          topologyIndex: 5
        },
        table: 'string',
        prefix: {
          topoObjectType: 'string',
          address: 'string',
          length: 5
        },
        pathCookie: 8,
        asPath: [
          'string',
          'string',
          'string'
        ],
        protocolNextHops: [
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocol: 'string',
        routePreference: 6,
        localPreference: 4,
        vpnLabel: 3
      }
    ];
    describe('#postV2TenantTenantIdStatisticsTeLspsBulkdelay - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsTeLspsBulkdelay(tenantTenantId, tenantPostV2TenantTenantIdStatisticsTeLspsBulkdelayBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdStatisticsTeLspsBulkdelay', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantLspName = 'fakedata';
    const tenantPostV2TenantTenantIdStatisticsTeLspsDelayDeviceNameLspNameBodyParam = [
      {
        topologyIndex: 8,
        topologyObjectType: 'string',
        node: {
          topoObjectType: 'string',
          nodeIndex: 3,
          id: 'string',
          topologyIndex: 7
        },
        table: 'string',
        prefix: {
          topoObjectType: 'string',
          address: 'string',
          length: 10
        },
        pathCookie: 9,
        asPath: [
          'string',
          'string'
        ],
        protocolNextHops: [
          'string',
          'string'
        ],
        protocol: 'string',
        routePreference: 9,
        localPreference: 8,
        vpnLabel: 10
      }
    ];
    describe('#postV2TenantTenantIdStatisticsTeLspsDelayDeviceNameLspName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsTeLspsDelayDeviceNameLspName(tenantTenantId, tenantDeviceName, tenantLspName, tenantPostV2TenantTenantIdStatisticsTeLspsDelayDeviceNameLspNameBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdStatisticsTeLspsDelayDeviceNameLspName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPostV2TenantTenantIdStatisticsTeLspsEventsBulkBodyParam = [
      {
        topologyIndex: 7,
        topologyObjectType: 'string',
        node: {
          topoObjectType: 'string',
          nodeIndex: 1,
          id: 'string',
          topologyIndex: 7
        },
        table: 'string',
        prefix: {
          topoObjectType: 'string',
          address: 'string',
          length: 1
        },
        pathCookie: 1,
        asPath: [
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocolNextHops: [
          'string',
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocol: 'string',
        routePreference: 6,
        localPreference: 2,
        vpnLabel: 2
      }
    ];
    describe('#postV2TenantTenantIdStatisticsTeLspsEventsBulk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsTeLspsEventsBulk(tenantTenantId, tenantPostV2TenantTenantIdStatisticsTeLspsEventsBulkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdStatisticsTeLspsEventsBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPostV2TenantTenantIdStatisticsTeLspsEventsLspNameBodyParam = [
      {
        topologyIndex: 3,
        topologyObjectType: 'string',
        node: {
          topoObjectType: 'string',
          nodeIndex: 5,
          id: 'string',
          topologyIndex: 3
        },
        table: 'string',
        prefix: {
          topoObjectType: 'string',
          address: 'string',
          length: 8
        },
        pathCookie: 7,
        asPath: [
          'string',
          'string'
        ],
        protocolNextHops: [
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocol: 'string',
        routePreference: 9,
        localPreference: 3,
        vpnLabel: 7
      }
    ];
    describe('#postV2TenantTenantIdStatisticsTeLspsEventsLspName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsTeLspsEventsLspName(tenantTenantId, tenantLspName, tenantPostV2TenantTenantIdStatisticsTeLspsEventsLspNameBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdStatisticsTeLspsEventsLspName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPostV2TenantTenantIdStatisticsTeLspsFieldsBodyParam = [
      {
        topologyIndex: 1,
        topologyObjectType: 'string',
        node: {
          topoObjectType: 'string',
          nodeIndex: 4,
          id: 'string',
          topologyIndex: 8
        },
        table: 'string',
        prefix: {
          topoObjectType: 'string',
          address: 'string',
          length: 6
        },
        pathCookie: 2,
        asPath: [
          'string',
          'string'
        ],
        protocolNextHops: [
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocol: 'string',
        routePreference: 10,
        localPreference: 1,
        vpnLabel: 6
      }
    ];
    describe('#postV2TenantTenantIdStatisticsTeLspsFields - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsTeLspsFields(tenantTenantId, tenantPostV2TenantTenantIdStatisticsTeLspsFieldsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdStatisticsTeLspsFields', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPostV2TenantTenantIdStatisticsTeLspsTopBodyParam = [
      {
        topologyIndex: 5,
        topologyObjectType: 'string',
        node: {
          topoObjectType: 'string',
          nodeIndex: 3,
          id: 'string',
          topologyIndex: 8
        },
        table: 'string',
        prefix: {
          topoObjectType: 'string',
          address: 'string',
          length: 7
        },
        pathCookie: 10,
        asPath: [
          'string'
        ],
        protocolNextHops: [
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocol: 'string',
        routePreference: 5,
        localPreference: 5,
        vpnLabel: 6
      }
    ];
    describe('#postV2TenantTenantIdStatisticsTeLspsTop - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsTeLspsTop(tenantTenantId, tenantPostV2TenantTenantIdStatisticsTeLspsTopBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdStatisticsTeLspsTop', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPostV2TenantTenantIdStatisticsTeLspsTrafficBodyParam = [
      {
        topologyIndex: 6,
        topologyObjectType: 'string',
        node: {
          topoObjectType: 'string',
          nodeIndex: 5,
          id: 'string',
          topologyIndex: 8
        },
        table: 'string',
        prefix: {
          topoObjectType: 'string',
          address: 'string',
          length: 8
        },
        pathCookie: 3,
        asPath: [
          'string',
          'string',
          'string'
        ],
        protocolNextHops: [
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocol: 'string',
        routePreference: 9,
        localPreference: 4,
        vpnLabel: 7
      }
    ];
    describe('#postV2TenantTenantIdStatisticsTeLspsTraffic - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsTeLspsTraffic(tenantTenantId, tenantPostV2TenantTenantIdStatisticsTeLspsTrafficBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdStatisticsTeLspsTraffic', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPostV2TenantTenantIdStatisticsTeLspsTrafficDeviceNameBodyParam = [
      {
        topologyIndex: 7,
        topologyObjectType: 'string',
        node: {
          topoObjectType: 'string',
          nodeIndex: 9,
          id: 'string',
          topologyIndex: 7
        },
        table: 'string',
        prefix: {
          topoObjectType: 'string',
          address: 'string',
          length: 2
        },
        pathCookie: 3,
        asPath: [
          'string',
          'string',
          'string',
          'string'
        ],
        protocolNextHops: [
          'string'
        ],
        protocol: 'string',
        routePreference: 2,
        localPreference: 9,
        vpnLabel: 8
      }
    ];
    describe('#postV2TenantTenantIdStatisticsTeLspsTrafficDeviceName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsTeLspsTrafficDeviceName(tenantTenantId, tenantDeviceName, tenantPostV2TenantTenantIdStatisticsTeLspsTrafficDeviceNameBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdStatisticsTeLspsTrafficDeviceName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPostV2TenantTenantIdStatisticsTeLspsTrafficDeviceNameLspNameBodyParam = [
      {
        topologyIndex: 1,
        topologyObjectType: 'string',
        node: {
          topoObjectType: 'string',
          nodeIndex: 3,
          id: 'string',
          topologyIndex: 7
        },
        table: 'string',
        prefix: {
          topoObjectType: 'string',
          address: 'string',
          length: 1
        },
        pathCookie: 10,
        asPath: [
          'string',
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        protocolNextHops: [
          'string',
          'string'
        ],
        protocol: 'string',
        routePreference: 9,
        localPreference: 5,
        vpnLabel: 9
      }
    ];
    describe('#postV2TenantTenantIdStatisticsTeLspsTrafficDeviceNameLspName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsTeLspsTrafficDeviceNameLspName(tenantTenantId, tenantDeviceName, tenantLspName, tenantPostV2TenantTenantIdStatisticsTeLspsTrafficDeviceNameLspNameBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdStatisticsTeLspsTrafficDeviceNameLspName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPostV2TenantTenantIdTopologyRpcOptimizeBodyParam = {
      elements: [
        {}
      ]
    };
    describe('#postV2TenantTenantIdTopologyRpcOptimize - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdTopologyRpcOptimize(tenantTenantId, tenantPostV2TenantTenantIdTopologyRpcOptimizeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.elements));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdTopologyRpcOptimize', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantTopologyId = 'fakedata';
    let tenantDemandIndex = 'fakedata';
    let tenantName = 'fakedata';
    const tenantPostV2TenantTenantIdTopologyTopologyIdDemandsBodyParam = {
      plannedProperties: {},
      name: 'string',
      from: {},
      pathType: 'string',
      to: {},
      demandIndex: 5,
      prefix: {}
    };
    describe('#postV2TenantTenantIdTopologyTopologyIdDemands - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdDemands(tenantTenantId, tenantTopologyId, tenantPostV2TenantTenantIdTopologyTopologyIdDemandsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.plannedProperties);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.from);
                assert.equal('string', data.response.pathType);
                assert.equal('object', typeof data.response.to);
                assert.equal(6, data.response.demandIndex);
                assert.equal('object', typeof data.response.prefix);
              } else {
                runCommonAsserts(data, error);
              }
              tenantDemandIndex = data.response.demandIndex;
              tenantName = data.response.name;
              saveMockData('Tenant', 'postV2TenantTenantIdTopologyTopologyIdDemands', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPostV2TenantTenantIdTopologyTopologyIdFacilitiesBodyParam = [
      {
        demandIndex: 3
      }
    ];
    describe('#postV2TenantTenantIdTopologyTopologyIdFacilities - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdFacilities(tenantTenantId, tenantTopologyId, tenantPostV2TenantTenantIdTopologyTopologyIdFacilitiesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdTopologyTopologyIdFacilities', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantEndANode = 'fakedata';
    const tenantEndZNode = 'fakedata';
    let tenantLinkIndex = 'fakedata';
    const tenantPostV2TenantTenantIdTopologyTopologyIdLinksBodyParam = {
      topoObjectType: 'string',
      topologyIndex: 10,
      name: 'string',
      operationalStatus: 'string',
      linkIndex: 5,
      endA: {
        topoObjectType: 'string'
      },
      endZ: {
        topoObjectType: 'string'
      }
    };
    describe('#postV2TenantTenantIdTopologyTopologyIdLinks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdLinks(tenantTenantId, tenantTopologyId, tenantEndANode, tenantEndZNode, 'fakedata', 'fakedata', tenantPostV2TenantTenantIdTopologyTopologyIdLinksBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.topoObjectType);
                assert.equal(10, data.response.topologyIndex);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.operationalStatus);
                assert.equal(9, data.response.linkIndex);
                assert.equal('object', typeof data.response.endA);
                assert.equal('object', typeof data.response.endZ);
              } else {
                runCommonAsserts(data, error);
              }
              tenantLinkIndex = data.response.linkIndex;
              saveMockData('Tenant', 'postV2TenantTenantIdTopologyTopologyIdLinks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let tenantTopologyIndex = 'fakedata';
    let tenantTopoObjectType = 'fakedata';
    let tenantMaintenanceIndex = 'fakedata';
    const tenantPostV2TenantTenantIdTopologyTopologyIdMaintenancesBodyParam = {
      topoObjectType: 'string',
      topologyIndex: 9,
      maintenanceIndex: 10,
      user: 'string',
      name: 'string',
      status: 'string',
      startTime: 'string',
      endTime: 'string',
      elements: [
        {
          topoObjectType: 'string'
        }
      ]
    };
    describe('#postV2TenantTenantIdTopologyTopologyIdMaintenances - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdMaintenances(tenantTenantId, tenantTopologyId, tenantPostV2TenantTenantIdTopologyTopologyIdMaintenancesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.topoObjectType);
                assert.equal(2, data.response.topologyIndex);
                assert.equal(3, data.response.maintenanceIndex);
                assert.equal('string', data.response.user);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.startTime);
                assert.equal('string', data.response.endTime);
                assert.equal(true, Array.isArray(data.response.elements));
              } else {
                runCommonAsserts(data, error);
              }
              tenantTopologyIndex = data.response.topologyIndex;
              tenantTopoObjectType = data.response.topoObjectType;
              tenantMaintenanceIndex = data.response.maintenanceIndex;
              saveMockData('Tenant', 'postV2TenantTenantIdTopologyTopologyIdMaintenances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPostV2TenantTenantIdTopologyTopologyIdNodesBodyParam = [
      {
        nodeType: [
          'string',
          'string',
          'string'
        ],
        node: 'string',
        clusterIPs: [
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        memory: {
          free: 6,
          usage: 10,
          total: 8
        },
        disk: {
          partitions: [
            {
              total: 3,
              used: 10,
              free: 2,
              usage: 3,
              partition: 'string'
            },
            {
              total: 5,
              used: 9,
              free: 4,
              usage: 8,
              partition: 'string'
            },
            {
              total: 6,
              used: 4,
              free: 8,
              usage: 7,
              partition: 'string'
            },
            {
              total: 9,
              used: 3,
              free: 4,
              usage: 1,
              partition: 'string'
            },
            {
              total: 4,
              used: 6,
              free: 8,
              usage: 2,
              partition: 'string'
            }
          ]
        },
        processes: [
          {
            timestamp: 'string',
            name: 'string',
            status: 'string',
            pid: 7,
            pcpu: 'string',
            rss: 1,
            vsz: 9,
            user: 'string',
            group: 'string',
            time: 'string',
            cmd: 'string'
          },
          {
            timestamp: 'string',
            name: 'string',
            status: 'string',
            pid: 2,
            pcpu: 'string',
            rss: 7,
            vsz: 1,
            user: 'string',
            group: 'string',
            time: 'string',
            cmd: 'string'
          },
          {
            timestamp: 'string',
            name: 'string',
            status: 'string',
            pid: 4,
            pcpu: 'string',
            rss: 5,
            vsz: 1,
            user: 'string',
            group: 'string',
            time: 'string',
            cmd: 'string'
          },
          {
            timestamp: 'string',
            name: 'string',
            status: 'string',
            pid: 1,
            pcpu: 'string',
            rss: 6,
            vsz: 9,
            user: 'string',
            group: 'string',
            time: 'string',
            cmd: 'string'
          },
          {
            timestamp: 'string',
            name: 'string',
            status: 'string',
            pid: 7,
            pcpu: 'string',
            rss: 7,
            vsz: 3,
            user: 'string',
            group: 'string',
            time: 'string',
            cmd: 'string'
          },
          {
            timestamp: 'string',
            name: 'string',
            status: 'string',
            pid: 1,
            pcpu: 'string',
            rss: 3,
            vsz: 7,
            user: 'string',
            group: 'string',
            time: 'string',
            cmd: 'string'
          },
          {
            timestamp: 'string',
            name: 'string',
            status: 'string',
            pid: 6,
            pcpu: 'string',
            rss: 7,
            vsz: 7,
            user: 'string',
            group: 'string',
            time: 'string',
            cmd: 'string'
          },
          {
            timestamp: 'string',
            name: 'string',
            status: 'string',
            pid: 5,
            pcpu: 'string',
            rss: 9,
            vsz: 6,
            user: 'string',
            group: 'string',
            time: 'string',
            cmd: 'string'
          },
          {
            timestamp: 'string',
            name: 'string',
            status: 'string',
            pid: 2,
            pcpu: 'string',
            rss: 4,
            vsz: 3,
            user: 'string',
            group: 'string',
            time: 'string',
            cmd: 'string'
          }
        ],
        time: 'string',
        role: [
          'string'
        ],
        CPU: {
          pcpu: 'string',
          cpus: [
            {
              pcpu: 'string'
            }
          ]
        },
        status: 'string'
      }
    ];
    describe('#postV2TenantTenantIdTopologyTopologyIdNodes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdNodes(tenantTenantId, tenantTopologyId, tenantName, tenantTopologyIndex, tenantTopoObjectType, tenantPostV2TenantTenantIdTopologyTopologyIdNodesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdTopologyTopologyIdNodes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantFromAddress = 'fakedata';
    const tenantPlannedPropertiesBandwidth = 'fakedata';
    const tenantPlannedPropertiesSetupPriority = 'fakedata';
    const tenantPlannedPropertiesHoldingPriority = 'fakedata';
    const tenantPlannedPropertiesDesignAdminGroupsAttributeIncludeAny = 'fakedata';
    const tenantPlannedPropertiesDesignAdminGroupsAttributeIncludeAll = 'fakedata';
    const tenantPlannedPropertiesDesignAdminGroupsAttributeExclude = 'fakedata';
    let tenantLsps = 'fakedata';
    const tenantPostV2TenantTenantIdTopologyTopologyIdP2mpBodyParam = {
      name: 'string',
      from: {
        topoObjectType: 'string'
      },
      topoObjectType: 'string',
      lsps: [
        {
          name: 'string',
          from: {
            topoObjectType: 'string'
          }
        }
      ]
    };
    describe('#postV2TenantTenantIdTopologyTopologyIdP2mp - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdP2mp(tenantTenantId, tenantTopologyId, tenantName, tenantFromAddress, tenantPlannedPropertiesBandwidth, tenantPlannedPropertiesSetupPriority, tenantPlannedPropertiesHoldingPriority, tenantPlannedPropertiesDesignAdminGroupsAttributeIncludeAny, tenantPlannedPropertiesDesignAdminGroupsAttributeIncludeAll, tenantPlannedPropertiesDesignAdminGroupsAttributeExclude, tenantLsps, tenantPostV2TenantTenantIdTopologyTopologyIdP2mpBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.from);
                assert.equal('string', data.response.topoObjectType);
                assert.equal(true, Array.isArray(data.response.lsps));
              } else {
                runCommonAsserts(data, error);
              }
              tenantLsps = data.response.lsps;
              saveMockData('Tenant', 'postV2TenantTenantIdTopologyTopologyIdP2mp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantP2mpGroupIndex = 'fakedata';
    const tenantPostV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndexBodyParam = [
      {
        plannedProperties: {
          bandwidth: 'string',
          setupPriority: 9,
          holdingPriority: 9,
          pathName: 'string',
          adminStatus: 'string'
        },
        name: 'string',
        from: {
          topoObjectType: 'string',
          address: 'string'
        },
        pathType: 'string',
        to: {
          topoObjectType: 'string',
          address: 'string'
        },
        lspIndex: 10,
        controlType: 'string',
        provisioningType: 'string',
        p2mpName: 'string'
      }
    ];
    describe('#postV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndex - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndex(tenantTenantId, tenantTopologyId, tenantP2mpGroupIndex, tenantPostV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndexBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPostV2TenantTenantIdTopologyTopologyIdPathComputationBodyParam = {
      result: 'string',
      responses: [
        {}
      ]
    };
    describe('#postV2TenantTenantIdTopologyTopologyIdPathComputation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdPathComputation(tenantTenantId, tenantTopologyId, tenantPostV2TenantTenantIdTopologyTopologyIdPathComputationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.result);
                assert.equal(true, Array.isArray(data.response.responses));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdTopologyTopologyIdPathComputation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPostV2TenantTenantIdTopologyTopologyIdTeContainersBodyParam = [
      {
        demandIndex: 1
      }
    ];
    describe('#postV2TenantTenantIdTopologyTopologyIdTeContainers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdTeContainers(tenantTenantId, tenantTopologyId, tenantPostV2TenantTenantIdTopologyTopologyIdTeContainersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdTopologyTopologyIdTeContainers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantToAddress = 'fakedata';
    const tenantPostV2TenantTenantIdTopologyTopologyIdTeLspsBodyParam = {
      name: 'string',
      from: {},
      to: {},
      pathType: 'string',
      plannedProperties: {}
    };
    describe('#postV2TenantTenantIdTopologyTopologyIdTeLsps - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdTeLsps(tenantTenantId, tenantTopologyId, tenantName, tenantFromAddress, tenantToAddress, tenantPostV2TenantTenantIdTopologyTopologyIdTeLspsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.from);
                assert.equal('object', typeof data.response.to);
                assert.equal('string', data.response.pathType);
                assert.equal('object', typeof data.response.plannedProperties);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'postV2TenantTenantIdTopologyTopologyIdTeLsps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let tenantTransportControllerIndex = 'fakedata';
    const tenantPostV2TenantTenantIdTransportControllersBodyParam = {
      name: 'string',
      interfaceType: 'string',
      topologyModel: 'string',
      notifyUrl: 'string',
      profileName: 'string',
      reconnectTimeout: 4,
      topologyUrl: 'string',
      topoObjectType: 'string',
      transportControllerIndex: 7
    };
    describe('#postV2TenantTenantIdTransportControllers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV2TenantTenantIdTransportControllers(tenantTenantId, tenantPostV2TenantTenantIdTransportControllersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.interfaceType);
                assert.equal('string', data.response.topologyModel);
                assert.equal('string', data.response.notifyUrl);
                assert.equal('string', data.response.profileName);
                assert.equal(6, data.response.reconnectTimeout);
                assert.equal('string', data.response.topologyUrl);
                assert.equal('string', data.response.topoObjectType);
                assert.equal(4, data.response.transportControllerIndex);
              } else {
                runCommonAsserts(data, error);
              }
              tenantTransportControllerIndex = data.response.transportControllerIndex;
              saveMockData('Tenant', 'postV2TenantTenantIdTransportControllers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdHealth - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV2TenantTenantIdHealth(tenantTenantId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.links));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'getV2TenantTenantIdHealth', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdHealthNodes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV2TenantTenantIdHealthNodes(tenantTenantId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'getV2TenantTenantIdHealthNodes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPassword = 'fakedata';
    const tenantPutV2TenantTenantIdHealthNodesSmtpconfigBodyParam = {
      host: 'string',
      secure: false,
      username: 'string',
      port: 4,
      enabled: false
    };
    describe('#putV2TenantTenantIdHealthNodesSmtpconfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putV2TenantTenantIdHealthNodesSmtpconfig(tenantTenantId, 'fakedata', 'fakedata', 'fakedata', 'fakedata', tenantPassword, 'fakedata', tenantPutV2TenantTenantIdHealthNodesSmtpconfigBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'putV2TenantTenantIdHealthNodesSmtpconfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdHealthNodesThresholds - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV2TenantTenantIdHealthNodesThresholds(tenantTenantId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'getV2TenantTenantIdHealthNodesThresholds', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPutV2TenantTenantIdHealthNodesThresholdsDiskUtilizationBodyParam = [
      {
        id: 'string',
        partition: 'string',
        node: 'string',
        critical: 'string',
        warning: 'string'
      }
    ];
    describe('#putV2TenantTenantIdHealthNodesThresholdsDiskUtilization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putV2TenantTenantIdHealthNodesThresholdsDiskUtilization(tenantTenantId, tenantPutV2TenantTenantIdHealthNodesThresholdsDiskUtilizationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'putV2TenantTenantIdHealthNodesThresholdsDiskUtilization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantNodeId = 'fakedata';
    describe('#getV2TenantTenantIdHealthNodesNodeId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV2TenantTenantIdHealthNodesNodeId(tenantTenantId, tenantNodeId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.nodeType));
                assert.equal('string', data.response.node);
                assert.equal(true, Array.isArray(data.response.clusterIPs));
                assert.equal('object', typeof data.response.memory);
                assert.equal('object', typeof data.response.disk);
                assert.equal(true, Array.isArray(data.response.processes));
                assert.equal('string', data.response.time);
                assert.equal(true, Array.isArray(data.response.role));
                assert.equal('object', typeof data.response.CPU);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'getV2TenantTenantIdHealthNodesNodeId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdHealthNodesNodeIdTime - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV2TenantTenantIdHealthNodesNodeIdTime(tenantTenantId, tenantNodeId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.node);
                assert.equal('string', data.response.time);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'getV2TenantTenantIdHealthNodesNodeIdTime', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantProcessName = 'fakedata';
    describe('#getV2TenantTenantIdHealthNodesNodeIdProcessName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV2TenantTenantIdHealthNodesNodeIdProcessName(tenantTenantId, tenantNodeId, tenantProcessName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.timestamp);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.status);
                assert.equal(1, data.response.pid);
                assert.equal('string', data.response.pcpu);
                assert.equal(6, data.response.rss);
                assert.equal(4, data.response.vsz);
                assert.equal('string', data.response.user);
                assert.equal('string', data.response.group);
                assert.equal('string', data.response.time);
                assert.equal('string', data.response.cmd);
                assert.equal(true, Array.isArray(data.response.history));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'getV2TenantTenantIdHealthNodesNodeIdProcessName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdHealthNodesNodeIdProcessNameHistory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV2TenantTenantIdHealthNodesNodeIdProcessNameHistory(tenantTenantId, tenantNodeId, tenantProcessName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'getV2TenantTenantIdHealthNodesNodeIdProcessNameHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPutV2TenantTenantIdHighAvailabilityHighAvailabilityBodyParam = [
      {
        preferenceType: 'string',
        host: 'string',
        priority: 10
      }
    ];
    describe('#putV2TenantTenantIdHighAvailabilityHighAvailability - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putV2TenantTenantIdHighAvailabilityHighAvailability(tenantTenantId, tenantPutV2TenantTenantIdHighAvailabilityHighAvailabilityBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'putV2TenantTenantIdHighAvailabilityHighAvailability', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdHighAvailabilityHosts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV2TenantTenantIdHighAvailabilityHosts(tenantTenantId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'getV2TenantTenantIdHighAvailabilityHosts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdHighAvailabilityZkstatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV2TenantTenantIdHighAvailabilityZkstatus(tenantTenantId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.master);
                assert.equal(true, Array.isArray(data.response.followers));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'getV2TenantTenantIdHighAvailabilityZkstatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantCollectionJobId = 'fakedata';
    describe('#getV2TenantTenantIdNetconfNetconfCollectionCollectionJobId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV2TenantTenantIdNetconfNetconfCollectionCollectionJobId(tenantTenantId, tenantCollectionJobId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'getV2TenantTenantIdNetconfNetconfCollectionCollectionJobId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdTopology - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV2TenantTenantIdTopology(tenantTenantId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'getV2TenantTenantIdTopology', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdTopologyRpcDiverseTreeDesign - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV2TenantTenantIdTopologyRpcDiverseTreeDesign(tenantTenantId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.topologyIndex);
                assert.equal(true, Array.isArray(data.response.rpcRequestIdList));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'getV2TenantTenantIdTopologyRpcDiverseTreeDesign', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantUuid = 'fakedata';
    describe('#getV2TenantTenantIdTopologyRpcDiverseTreeDesignUuid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV2TenantTenantIdTopologyRpcDiverseTreeDesignUuid(tenantTenantId, tenantUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.result);
                assert.equal('object', typeof data.response.request);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'getV2TenantTenantIdTopologyRpcDiverseTreeDesignUuid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdTopologyRpcSimulation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV2TenantTenantIdTopologyRpcSimulation(tenantTenantId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.topologyIndex);
                assert.equal(true, Array.isArray(data.response.simulationReports));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'getV2TenantTenantIdTopologyRpcSimulation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdTopologyRpcSimulationUuid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV2TenantTenantIdTopologyRpcSimulationUuid(tenantTenantId, tenantUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.result);
                assert.equal('object', typeof data.response.request);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'getV2TenantTenantIdTopologyRpcSimulationUuid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantReportName = 'fakedata';
    describe('#getV2TenantTenantIdTopologyRpcSimulationUuidReportName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV2TenantTenantIdTopologyRpcSimulationUuidReportName(tenantTenantId, tenantUuid, tenantReportName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.simulationId);
                assert.equal(4, data.response.topologyIndex);
                assert.equal(true, Array.isArray(data.response.elements));
                assert.equal(true, Array.isArray(data.response.reports));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'getV2TenantTenantIdTopologyRpcSimulationUuidReportName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantNameOptional = 'fakedata';
    const tenantAddressOptional = 'fakedata';
    const tenantQueryTypeOptional = 'fakedata';
    describe('#getV2TenantTenantIdTopologyTopologyIdLinksSearch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdLinksSearch(tenantTenantId, tenantTopologyId, tenantNameOptional, tenantAddressOptional, tenantQueryTypeOptional, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'getV2TenantTenantIdTopologyTopologyIdLinksSearch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantStartOptional = 'fakedata';
    const tenantEndOptional = 'fakedata';
    describe('#getV2TenantTenantIdTopologyTopologyIdLinksLinkIndexHistory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdLinksLinkIndexHistory(tenantTenantId, tenantTopologyId, tenantLinkIndex, tenantStartOptional, tenantEndOptional, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'getV2TenantTenantIdTopologyTopologyIdLinksLinkIndexHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantHostNameOptional = 'fakedata';
    const tenantASOptional = 'fakedata';
    describe('#getV2TenantTenantIdTopologyTopologyIdNodesSearch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdNodesSearch(tenantTenantId, tenantTopologyId, tenantNameOptional, tenantHostNameOptional, tenantASOptional, tenantQueryTypeOptional, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'getV2TenantTenantIdTopologyTopologyIdNodesSearch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantNodeIndex = 'fakedata';
    describe('#getV2TenantTenantIdTopologyTopologyIdNodesNodeIndexHistory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdNodesNodeIndexHistory(tenantTenantId, tenantTopologyId, tenantNodeIndex, tenantStartOptional, tenantEndOptional, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'getV2TenantTenantIdTopologyTopologyIdNodesNodeIndexHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdTopologyTopologyIdRoutes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdRoutes(tenantTenantId, tenantTopologyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'getV2TenantTenantIdTopologyTopologyIdRoutes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdTopologyTopologyIdRoutesNodeIndex - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdRoutesNodeIndex(tenantTenantId, tenantTopologyId, tenantNodeIndex, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'getV2TenantTenantIdTopologyTopologyIdRoutesNodeIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdTopologyTopologyIdStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdStatus(tenantTenantId, tenantTopologyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'getV2TenantTenantIdTopologyTopologyIdStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdTopologyTopologyIdStatusAnalyticsCollection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdStatusAnalyticsCollection(tenantTenantId, tenantTopologyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.component);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.statusTimestampTime);
                assert.equal(3, data.response.statusTimestamp);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'getV2TenantTenantIdTopologyTopologyIdStatusAnalyticsCollection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdTopologyTopologyIdStatusAnalyticsDatabase - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdStatusAnalyticsDatabase(tenantTenantId, tenantTopologyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.component);
                assert.equal('string', data.response.status);
                assert.equal(2, data.response.statusTimestamp);
                assert.equal('string', data.response.statusTimestampTime);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'getV2TenantTenantIdTopologyTopologyIdStatusAnalyticsDatabase', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdTopologyTopologyIdStatusPathComputationServer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdStatusPathComputationServer(tenantTenantId, tenantTopologyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.component);
                assert.equal('string', data.response.status);
                assert.equal(6, data.response.statusTimestamp);
                assert.equal('string', data.response.statusTimestampTime);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'getV2TenantTenantIdTopologyTopologyIdStatusPathComputationServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdTopologyTopologyIdStatusPce - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdStatusPce(tenantTenantId, tenantTopologyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.component);
                assert.equal('string', data.response.status);
                assert.equal(10, data.response.statusTimestamp);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'getV2TenantTenantIdTopologyTopologyIdStatusPce', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdTopologyTopologyIdStatusTopologyAcquisition - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdStatusTopologyAcquisition(tenantTenantId, tenantTopologyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.component);
                assert.equal('string', data.response.status);
                assert.equal(9, data.response.statusTimestamp);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'getV2TenantTenantIdTopologyTopologyIdStatusTopologyAcquisition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdTopologyTopologyIdStatusTransportTopologyAcquisition - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdStatusTransportTopologyAcquisition(tenantTenantId, tenantTopologyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.component);
                assert.equal('string', data.response.status);
                assert.equal(true, Array.isArray(data.response.childs));
                assert.equal(7, data.response.statusTimestamp);
                assert.equal('string', data.response.statusTimestampTime);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'getV2TenantTenantIdTopologyTopologyIdStatusTransportTopologyAcquisition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantFromOptional = 'fakedata';
    const tenantOperationalStatusOptional = 'fakedata';
    describe('#getV2TenantTenantIdTopologyTopologyIdTeLspsSearch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdTeLspsSearch(tenantTenantId, tenantTopologyId, tenantNameOptional, tenantFromOptional, tenantOperationalStatusOptional, tenantQueryTypeOptional, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'getV2TenantTenantIdTopologyTopologyIdTeLspsSearch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantLspIndex = 'fakedata';
    describe('#getV2TenantTenantIdTopologyTopologyIdTeLspsLspIndexHistory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdTeLspsLspIndexHistory(tenantTenantId, tenantTopologyId, tenantLspIndex, tenantStartOptional, tenantEndOptional, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'getV2TenantTenantIdTopologyTopologyIdTeLspsLspIndexHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let tenantHost = 'fakedata';
    let tenantPort = 'fakedata';
    let tenantSecure = 'fakedata';
    let tenantUsername = 'fakedata';
    let tenantEnabled = 'fakedata';
    describe('#deleteV2TenantTenantIdHealthNodesSubscribers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteV2TenantTenantIdHealthNodesSubscribers(tenantTenantId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              tenantHost = data.response.host;
              tenantPort = data.response.port;
              tenantSecure = data.response.secure;
              tenantUsername = data.response.username;
              tenantEnabled = data.response.enabled;
              saveMockData('Tenant', 'deleteV2TenantTenantIdHealthNodesSubscribers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV2TenantTenantIdNetconfProfiles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteV2TenantTenantIdNetconfProfiles(tenantTenantId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'deleteV2TenantTenantIdNetconfProfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV2TenantTenantIdTopologyTopologyId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyId(tenantTenantId, tenantTopologyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'deleteV2TenantTenantIdTopologyTopologyId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV2TenantTenantIdTopologyTopologyIdDemandsBulk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdDemandsBulk(tenantTenantId, tenantTopologyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'deleteV2TenantTenantIdTopologyTopologyIdDemandsBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV2TenantTenantIdTopologyTopologyIdDemandsDemandIndex - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdDemandsDemandIndex(tenantTenantId, tenantTopologyId, tenantDemandIndex, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'deleteV2TenantTenantIdTopologyTopologyIdDemandsDemandIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantFacilityIndex = 'fakedata';
    describe('#deleteV2TenantTenantIdTopologyTopologyIdFacilitiesFacilityIndex - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdFacilitiesFacilityIndex(tenantTenantId, tenantTopologyId, tenantFacilityIndex, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'deleteV2TenantTenantIdTopologyTopologyIdFacilitiesFacilityIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV2TenantTenantIdTopologyTopologyIdLinksLinkIndex - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdLinksLinkIndex(tenantTenantId, tenantTopologyId, tenantLinkIndex, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'deleteV2TenantTenantIdTopologyTopologyIdLinksLinkIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV2TenantTenantIdTopologyTopologyIdMaintenancesMaintenanceIndex - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdMaintenancesMaintenanceIndex(tenantTenantId, tenantTopologyId, tenantMaintenanceIndex, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'deleteV2TenantTenantIdTopologyTopologyIdMaintenancesMaintenanceIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV2TenantTenantIdTopologyTopologyIdNodesNodeIndex - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdNodesNodeIndex(tenantTenantId, tenantTopologyId, tenantNodeIndex, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'deleteV2TenantTenantIdTopologyTopologyIdNodesNodeIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV2TenantTenantIdTopologyTopologyIdP2mpBulk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdP2mpBulk(tenantTenantId, tenantTopologyId, tenantP2mpGroupIndex, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'deleteV2TenantTenantIdTopologyTopologyIdP2mpBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndexBulk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndexBulk(tenantTenantId, tenantTopologyId, tenantP2mpGroupIndex, tenantLspIndex, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'deleteV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndexBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndexLspIndex - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndexLspIndex(tenantTenantId, tenantTopologyId, tenantP2mpGroupIndex, tenantLspIndex, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'deleteV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndexLspIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV2TenantTenantIdTopologyTopologyIdTeContainersBulk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdTeContainersBulk(tenantTenantId, tenantTopologyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'deleteV2TenantTenantIdTopologyTopologyIdTeContainersBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantContainerIndex = 'fakedata';
    describe('#deleteV2TenantTenantIdTopologyTopologyIdTeContainersContainerIndex - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdTeContainersContainerIndex(tenantTenantId, tenantTopologyId, tenantContainerIndex, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'deleteV2TenantTenantIdTopologyTopologyIdTeContainersContainerIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV2TenantTenantIdTopologyTopologyIdTeLspsBulk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdTeLspsBulk(tenantTenantId, tenantTopologyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'deleteV2TenantTenantIdTopologyTopologyIdTeLspsBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV2TenantTenantIdTopologyTopologyIdTeLspsLspIndex - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdTeLspsLspIndex(tenantTenantId, tenantTopologyId, tenantLspIndex, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'deleteV2TenantTenantIdTopologyTopologyIdTeLspsLspIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV2TenantTenantIdTransportControllerGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteV2TenantTenantIdTransportControllerGroups(tenantTenantId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'deleteV2TenantTenantIdTransportControllerGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantTransportControllerGroupName = 'fakedata';
    describe('#deleteV2TenantTenantIdTransportControllerGroupsTransportControllerGroupName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteV2TenantTenantIdTransportControllerGroupsTransportControllerGroupName(tenantTenantId, tenantTransportControllerGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'deleteV2TenantTenantIdTransportControllerGroupsTransportControllerGroupName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV2TenantTenantIdTransportControllersTransportControllerIndex - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteV2TenantTenantIdTransportControllersTransportControllerIndex(tenantTenantId, tenantTransportControllerIndex, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'deleteV2TenantTenantIdTransportControllersTransportControllerIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
