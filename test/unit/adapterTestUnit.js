/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-juniper_northstar',
      type: 'JuniperNorthstar',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const JuniperNorthstar = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Juniper-northstar Adapter Test', () => {
  describe('JuniperNorthstar Class Tests', () => {
    const a = new JuniperNorthstar(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('juniper_northstar'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('juniper_northstar'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('JuniperNorthstar', pronghornDotJson.export);
          assert.equal('Juniper-northstar', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-juniper_northstar', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('juniper_northstar'));
          assert.equal('JuniperNorthstar', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-juniper_northstar', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-juniper_northstar', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#getV2TenantTenantIdTopology - errors', () => {
      it('should have a getV2TenantTenantIdTopology function', (done) => {
        try {
          assert.equal(true, typeof a.getV2TenantTenantIdTopology === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getV2TenantTenantIdTopology(null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopology', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV2TenantTenantIdTopologyTopologyId - errors', () => {
      it('should have a deleteV2TenantTenantIdTopologyTopologyId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV2TenantTenantIdTopologyTopologyId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyId(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdTopologyTopologyIdStatus - errors', () => {
      it('should have a getV2TenantTenantIdTopologyTopologyIdStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getV2TenantTenantIdTopologyTopologyIdStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdStatus(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdStatus('fakeparam', null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdTopologyTopologyIdStatusPce - errors', () => {
      it('should have a getV2TenantTenantIdTopologyTopologyIdStatusPce function', (done) => {
        try {
          assert.equal(true, typeof a.getV2TenantTenantIdTopologyTopologyIdStatusPce === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdStatusPce(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdStatusPce', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdStatusPce('fakeparam', null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdStatusPce', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdTopologyTopologyIdStatusTopologyAcquisition - errors', () => {
      it('should have a getV2TenantTenantIdTopologyTopologyIdStatusTopologyAcquisition function', (done) => {
        try {
          assert.equal(true, typeof a.getV2TenantTenantIdTopologyTopologyIdStatusTopologyAcquisition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdStatusTopologyAcquisition(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdStatusTopologyAcquisition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdStatusTopologyAcquisition('fakeparam', null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdStatusTopologyAcquisition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdTopologyTopologyIdStatusPathComputationServer - errors', () => {
      it('should have a getV2TenantTenantIdTopologyTopologyIdStatusPathComputationServer function', (done) => {
        try {
          assert.equal(true, typeof a.getV2TenantTenantIdTopologyTopologyIdStatusPathComputationServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdStatusPathComputationServer(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdStatusPathComputationServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdStatusPathComputationServer('fakeparam', null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdStatusPathComputationServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdTopologyTopologyIdStatusTransportTopologyAcquisition - errors', () => {
      it('should have a getV2TenantTenantIdTopologyTopologyIdStatusTransportTopologyAcquisition function', (done) => {
        try {
          assert.equal(true, typeof a.getV2TenantTenantIdTopologyTopologyIdStatusTransportTopologyAcquisition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdStatusTransportTopologyAcquisition(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdStatusTransportTopologyAcquisition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdStatusTransportTopologyAcquisition('fakeparam', null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdStatusTransportTopologyAcquisition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdTopologyTopologyIdStatusAnalyticsCollection - errors', () => {
      it('should have a getV2TenantTenantIdTopologyTopologyIdStatusAnalyticsCollection function', (done) => {
        try {
          assert.equal(true, typeof a.getV2TenantTenantIdTopologyTopologyIdStatusAnalyticsCollection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdStatusAnalyticsCollection(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdStatusAnalyticsCollection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdStatusAnalyticsCollection('fakeparam', null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdStatusAnalyticsCollection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdTopologyTopologyIdStatusAnalyticsDatabase - errors', () => {
      it('should have a getV2TenantTenantIdTopologyTopologyIdStatusAnalyticsDatabase function', (done) => {
        try {
          assert.equal(true, typeof a.getV2TenantTenantIdTopologyTopologyIdStatusAnalyticsDatabase === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdStatusAnalyticsDatabase(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdStatusAnalyticsDatabase', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdStatusAnalyticsDatabase('fakeparam', null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdStatusAnalyticsDatabase', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdTopologyTopologyIdNodes - errors', () => {
      it('should have a postV2TenantTenantIdTopologyTopologyIdNodes function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdTopologyTopologyIdNodes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdNodes(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdNodes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdNodes('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdNodes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdNodes('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdNodes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyIndex', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdNodes('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'topologyIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdNodes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topoObjectType', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdNodes('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'topoObjectType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdNodes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdTopologyTopologyIdNodesSearch - errors', () => {
      it('should have a getV2TenantTenantIdTopologyTopologyIdNodesSearch function', (done) => {
        try {
          assert.equal(true, typeof a.getV2TenantTenantIdTopologyTopologyIdNodesSearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdNodesSearch(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdNodesSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdNodesSearch('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdNodesSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nameOptional', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdNodesSearch('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'nameOptional is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdNodesSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing hostNameOptional', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdNodesSearch('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'hostNameOptional is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdNodesSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing aSOptional', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdNodesSearch('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'aSOptional is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdNodesSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing queryTypeOptional', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdNodesSearch('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'queryTypeOptional is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdNodesSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV2TenantTenantIdTopologyTopologyIdNodesNodeIndex - errors', () => {
      it('should have a deleteV2TenantTenantIdTopologyTopologyIdNodesNodeIndex function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV2TenantTenantIdTopologyTopologyIdNodesNodeIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdNodesNodeIndex(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdNodesNodeIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdNodesNodeIndex('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdNodesNodeIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeIndex', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdNodesNodeIndex('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdNodesNodeIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdTopologyTopologyIdNodesNodeIndexHistory - errors', () => {
      it('should have a getV2TenantTenantIdTopologyTopologyIdNodesNodeIndexHistory function', (done) => {
        try {
          assert.equal(true, typeof a.getV2TenantTenantIdTopologyTopologyIdNodesNodeIndexHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdNodesNodeIndexHistory(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdNodesNodeIndexHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdNodesNodeIndexHistory('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdNodesNodeIndexHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeIndex', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdNodesNodeIndexHistory('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'nodeIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdNodesNodeIndexHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startOptional', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdNodesNodeIndexHistory('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'startOptional is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdNodesNodeIndexHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endOptional', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdNodesNodeIndexHistory('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'endOptional is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdNodesNodeIndexHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdTopologyTopologyIdLinks - errors', () => {
      it('should have a postV2TenantTenantIdTopologyTopologyIdLinks function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdTopologyTopologyIdLinks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdLinks(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdLinks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdLinks('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdLinks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endANode', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdLinks('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endANode is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdLinks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endZNode', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdLinks('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'endZNode is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdLinks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyIndex', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdLinks('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'topologyIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdLinks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topoObjectType', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdLinks('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'topoObjectType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdLinks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdTopologyTopologyIdLinksSearch - errors', () => {
      it('should have a getV2TenantTenantIdTopologyTopologyIdLinksSearch function', (done) => {
        try {
          assert.equal(true, typeof a.getV2TenantTenantIdTopologyTopologyIdLinksSearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdLinksSearch(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdLinksSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdLinksSearch('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdLinksSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nameOptional', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdLinksSearch('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'nameOptional is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdLinksSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing addressOptional', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdLinksSearch('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'addressOptional is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdLinksSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing queryTypeOptional', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdLinksSearch('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'queryTypeOptional is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdLinksSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV2TenantTenantIdTopologyTopologyIdLinksLinkIndex - errors', () => {
      it('should have a deleteV2TenantTenantIdTopologyTopologyIdLinksLinkIndex function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV2TenantTenantIdTopologyTopologyIdLinksLinkIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdLinksLinkIndex(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdLinksLinkIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdLinksLinkIndex('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdLinksLinkIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkIndex', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdLinksLinkIndex('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'linkIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdLinksLinkIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdTopologyTopologyIdLinksLinkIndexHistory - errors', () => {
      it('should have a getV2TenantTenantIdTopologyTopologyIdLinksLinkIndexHistory function', (done) => {
        try {
          assert.equal(true, typeof a.getV2TenantTenantIdTopologyTopologyIdLinksLinkIndexHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdLinksLinkIndexHistory(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdLinksLinkIndexHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdLinksLinkIndexHistory('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdLinksLinkIndexHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkIndex', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdLinksLinkIndexHistory('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'linkIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdLinksLinkIndexHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startOptional', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdLinksLinkIndexHistory('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'startOptional is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdLinksLinkIndexHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endOptional', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdLinksLinkIndexHistory('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'endOptional is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdLinksLinkIndexHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdTopologyTopologyIdTeLsps - errors', () => {
      it('should have a postV2TenantTenantIdTopologyTopologyIdTeLsps function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdTopologyTopologyIdTeLsps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdTeLsps(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdTeLsps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdTeLsps('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdTeLsps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdTeLsps('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdTeLsps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fromAddress', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdTeLsps('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'fromAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdTeLsps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing toAddress', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdTeLsps('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'toAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdTeLsps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdTopologyTopologyIdTeLspsSearch - errors', () => {
      it('should have a getV2TenantTenantIdTopologyTopologyIdTeLspsSearch function', (done) => {
        try {
          assert.equal(true, typeof a.getV2TenantTenantIdTopologyTopologyIdTeLspsSearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdTeLspsSearch(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdTeLspsSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdTeLspsSearch('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdTeLspsSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nameOptional', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdTeLspsSearch('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'nameOptional is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdTeLspsSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fromOptional', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdTeLspsSearch('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'fromOptional is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdTeLspsSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing operationalStatusOptional', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdTeLspsSearch('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'operationalStatusOptional is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdTeLspsSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing queryTypeOptional', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdTeLspsSearch('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'queryTypeOptional is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdTeLspsSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV2TenantTenantIdTopologyTopologyIdTeLspsLspIndex - errors', () => {
      it('should have a deleteV2TenantTenantIdTopologyTopologyIdTeLspsLspIndex function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV2TenantTenantIdTopologyTopologyIdTeLspsLspIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdTeLspsLspIndex(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdTeLspsLspIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdTeLspsLspIndex('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdTeLspsLspIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lspIndex', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdTeLspsLspIndex('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'lspIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdTeLspsLspIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV2TenantTenantIdTopologyTopologyIdTeLspsBulk - errors', () => {
      it('should have a deleteV2TenantTenantIdTopologyTopologyIdTeLspsBulk function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV2TenantTenantIdTopologyTopologyIdTeLspsBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdTeLspsBulk(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdTeLspsBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdTeLspsBulk('fakeparam', null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdTeLspsBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdTopologyTopologyIdTeLspsLspIndexHistory - errors', () => {
      it('should have a getV2TenantTenantIdTopologyTopologyIdTeLspsLspIndexHistory function', (done) => {
        try {
          assert.equal(true, typeof a.getV2TenantTenantIdTopologyTopologyIdTeLspsLspIndexHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdTeLspsLspIndexHistory(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdTeLspsLspIndexHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdTeLspsLspIndexHistory('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdTeLspsLspIndexHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lspIndex', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdTeLspsLspIndexHistory('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'lspIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdTeLspsLspIndexHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startOptional', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdTeLspsLspIndexHistory('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'startOptional is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdTeLspsLspIndexHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endOptional', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdTeLspsLspIndexHistory('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'endOptional is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdTeLspsLspIndexHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdTopologyTopologyIdDemands - errors', () => {
      it('should have a postV2TenantTenantIdTopologyTopologyIdDemands function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdTopologyTopologyIdDemands === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdDemands(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdDemands', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdDemands('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdDemands', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV2TenantTenantIdTopologyTopologyIdDemandsDemandIndex - errors', () => {
      it('should have a deleteV2TenantTenantIdTopologyTopologyIdDemandsDemandIndex function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV2TenantTenantIdTopologyTopologyIdDemandsDemandIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdDemandsDemandIndex(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdDemandsDemandIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdDemandsDemandIndex('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdDemandsDemandIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing demandIndex', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdDemandsDemandIndex('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'demandIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdDemandsDemandIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV2TenantTenantIdTopologyTopologyIdDemandsBulk - errors', () => {
      it('should have a deleteV2TenantTenantIdTopologyTopologyIdDemandsBulk function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV2TenantTenantIdTopologyTopologyIdDemandsBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdDemandsBulk(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdDemandsBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdDemandsBulk('fakeparam', null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdDemandsBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV2TenantTenantIdNetconfProfiles - errors', () => {
      it('should have a deleteV2TenantTenantIdNetconfProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV2TenantTenantIdNetconfProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.deleteV2TenantTenantIdNetconfProfiles(null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdNetconfProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdNetconfNetconfCollectionLiveNetwork - errors', () => {
      it('should have a postV2TenantTenantIdNetconfNetconfCollectionLiveNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdNetconfNetconfCollectionLiveNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdNetconfNetconfCollectionLiveNetwork(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdNetconfNetconfCollectionLiveNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdNetconfNetconfCollectionCollectionJobId - errors', () => {
      it('should have a getV2TenantTenantIdNetconfNetconfCollectionCollectionJobId function', (done) => {
        try {
          assert.equal(true, typeof a.getV2TenantTenantIdNetconfNetconfCollectionCollectionJobId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getV2TenantTenantIdNetconfNetconfCollectionCollectionJobId(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdNetconfNetconfCollectionCollectionJobId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing collectionJobId', (done) => {
        try {
          a.getV2TenantTenantIdNetconfNetconfCollectionCollectionJobId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'collectionJobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdNetconfNetconfCollectionCollectionJobId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdTopologyTopologyIdTeContainers - errors', () => {
      it('should have a postV2TenantTenantIdTopologyTopologyIdTeContainers function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdTopologyTopologyIdTeContainers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdTeContainers(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdTeContainers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdTeContainers('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdTeContainers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV2TenantTenantIdTopologyTopologyIdTeContainersContainerIndex - errors', () => {
      it('should have a deleteV2TenantTenantIdTopologyTopologyIdTeContainersContainerIndex function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV2TenantTenantIdTopologyTopologyIdTeContainersContainerIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdTeContainersContainerIndex(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdTeContainersContainerIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdTeContainersContainerIndex('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdTeContainersContainerIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerIndex', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdTeContainersContainerIndex('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'containerIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdTeContainersContainerIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV2TenantTenantIdTopologyTopologyIdTeContainersBulk - errors', () => {
      it('should have a deleteV2TenantTenantIdTopologyTopologyIdTeContainersBulk function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV2TenantTenantIdTopologyTopologyIdTeContainersBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdTeContainersBulk(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdTeContainersBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdTeContainersBulk('fakeparam', null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdTeContainersBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdTopologyTopologyIdFacilities - errors', () => {
      it('should have a postV2TenantTenantIdTopologyTopologyIdFacilities function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdTopologyTopologyIdFacilities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdFacilities(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdFacilities', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdFacilities('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdFacilities', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV2TenantTenantIdTopologyTopologyIdFacilitiesFacilityIndex - errors', () => {
      it('should have a deleteV2TenantTenantIdTopologyTopologyIdFacilitiesFacilityIndex function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV2TenantTenantIdTopologyTopologyIdFacilitiesFacilityIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdFacilitiesFacilityIndex(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdFacilitiesFacilityIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdFacilitiesFacilityIndex('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdFacilitiesFacilityIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing facilityIndex', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdFacilitiesFacilityIndex('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'facilityIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdFacilitiesFacilityIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdTopologyTopologyIdP2mp - errors', () => {
      it('should have a postV2TenantTenantIdTopologyTopologyIdP2mp function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdTopologyTopologyIdP2mp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdP2mp(null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdP2mp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdP2mp('fakeparam', null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdP2mp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdP2mp('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdP2mp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fromAddress', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdP2mp('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'fromAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdP2mp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing plannedPropertiesBandwidth', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdP2mp('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'plannedPropertiesBandwidth is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdP2mp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing plannedPropertiesSetupPriority', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdP2mp('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'plannedPropertiesSetupPriority is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdP2mp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing plannedPropertiesHoldingPriority', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdP2mp('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'plannedPropertiesHoldingPriority is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdP2mp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing plannedPropertiesDesignAdminGroupsAttributeIncludeAny', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdP2mp('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'plannedPropertiesDesignAdminGroupsAttributeIncludeAny is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdP2mp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing plannedPropertiesDesignAdminGroupsAttributeIncludeAll', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdP2mp('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'plannedPropertiesDesignAdminGroupsAttributeIncludeAll is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdP2mp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing plannedPropertiesDesignAdminGroupsAttributeExclude', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdP2mp('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'plannedPropertiesDesignAdminGroupsAttributeExclude is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdP2mp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lsps', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdP2mp('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'lsps is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdP2mp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndex - errors', () => {
      it('should have a postV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndex function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndex(null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndex('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing p2mpGroupIndex', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndex('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'p2mpGroupIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV2TenantTenantIdTopologyTopologyIdP2mpBulk - errors', () => {
      it('should have a deleteV2TenantTenantIdTopologyTopologyIdP2mpBulk function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV2TenantTenantIdTopologyTopologyIdP2mpBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdP2mpBulk(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdP2mpBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdP2mpBulk('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdP2mpBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing p2mpGroupIndex', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdP2mpBulk('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'p2mpGroupIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdP2mpBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndexLspIndex - errors', () => {
      it('should have a deleteV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndexLspIndex function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndexLspIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndexLspIndex(null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndexLspIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndexLspIndex('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndexLspIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing p2mpGroupIndex', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndexLspIndex('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'p2mpGroupIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndexLspIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lspIndex', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndexLspIndex('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'lspIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndexLspIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndexBulk - errors', () => {
      it('should have a deleteV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndexBulk function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndexBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndexBulk(null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndexBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndexBulk('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndexBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing p2mpGroupIndex', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndexBulk('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'p2mpGroupIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndexBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lspIndex', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndexBulk('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'lspIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndexBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdTopologyTopologyIdRoutes - errors', () => {
      it('should have a getV2TenantTenantIdTopologyTopologyIdRoutes function', (done) => {
        try {
          assert.equal(true, typeof a.getV2TenantTenantIdTopologyTopologyIdRoutes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdRoutes(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdRoutes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdRoutes('fakeparam', null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdRoutes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdTopologyTopologyIdRoutesNodeIndex - errors', () => {
      it('should have a getV2TenantTenantIdTopologyTopologyIdRoutesNodeIndex function', (done) => {
        try {
          assert.equal(true, typeof a.getV2TenantTenantIdTopologyTopologyIdRoutesNodeIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdRoutesNodeIndex(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdRoutesNodeIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdRoutesNodeIndex('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdRoutesNodeIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeIndex', (done) => {
        try {
          a.getV2TenantTenantIdTopologyTopologyIdRoutesNodeIndex('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyTopologyIdRoutesNodeIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdStatisticsDemandsBulk - errors', () => {
      it('should have a postV2TenantTenantIdStatisticsDemandsBulk function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdStatisticsDemandsBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsDemandsBulk(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsDemandsBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdStatisticsInterfacesDelayDeviceNameInterfaceName - errors', () => {
      it('should have a postV2TenantTenantIdStatisticsInterfacesDelayDeviceNameInterfaceName function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdStatisticsInterfacesDelayDeviceNameInterfaceName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsInterfacesDelayDeviceNameInterfaceName(null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsInterfacesDelayDeviceNameInterfaceName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceName', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsInterfacesDelayDeviceNameInterfaceName('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'deviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsInterfacesDelayDeviceNameInterfaceName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceName', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsInterfacesDelayDeviceNameInterfaceName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'interfaceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsInterfacesDelayDeviceNameInterfaceName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdStatisticsInterfacesBulk - errors', () => {
      it('should have a postV2TenantTenantIdStatisticsInterfacesBulk function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdStatisticsInterfacesBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsInterfacesBulk(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsInterfacesBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdStatisticsInterfacesFields - errors', () => {
      it('should have a postV2TenantTenantIdStatisticsInterfacesFields function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdStatisticsInterfacesFields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsInterfacesFields(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsInterfacesFields', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdStatisticsInterfacesTop - errors', () => {
      it('should have a postV2TenantTenantIdStatisticsInterfacesTop function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdStatisticsInterfacesTop === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsInterfacesTop(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsInterfacesTop', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdStatisticsInterfacesChildtrafficDeviceNameInterfaceName - errors', () => {
      it('should have a postV2TenantTenantIdStatisticsInterfacesChildtrafficDeviceNameInterfaceName function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdStatisticsInterfacesChildtrafficDeviceNameInterfaceName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsInterfacesChildtrafficDeviceNameInterfaceName(null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsInterfacesChildtrafficDeviceNameInterfaceName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceName', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsInterfacesChildtrafficDeviceNameInterfaceName('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'deviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsInterfacesChildtrafficDeviceNameInterfaceName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceName', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsInterfacesChildtrafficDeviceNameInterfaceName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'interfaceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsInterfacesChildtrafficDeviceNameInterfaceName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdStatisticsInterfacesBulkdelay - errors', () => {
      it('should have a postV2TenantTenantIdStatisticsInterfacesBulkdelay function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdStatisticsInterfacesBulkdelay === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsInterfacesBulkdelay(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsInterfacesBulkdelay', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdStatisticsInterfacesTraffic - errors', () => {
      it('should have a postV2TenantTenantIdStatisticsInterfacesTraffic function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdStatisticsInterfacesTraffic === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsInterfacesTraffic(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsInterfacesTraffic', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdStatisticsInterfacesTrafficDeviceName - errors', () => {
      it('should have a postV2TenantTenantIdStatisticsInterfacesTrafficDeviceName function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdStatisticsInterfacesTrafficDeviceName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsInterfacesTrafficDeviceName(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsInterfacesTrafficDeviceName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceName', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsInterfacesTrafficDeviceName('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'deviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsInterfacesTrafficDeviceName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdStatisticsInterfacesTrafficDeviceNameInterfaceName - errors', () => {
      it('should have a postV2TenantTenantIdStatisticsInterfacesTrafficDeviceNameInterfaceName function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdStatisticsInterfacesTrafficDeviceNameInterfaceName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsInterfacesTrafficDeviceNameInterfaceName(null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsInterfacesTrafficDeviceNameInterfaceName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceName', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsInterfacesTrafficDeviceNameInterfaceName('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'deviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsInterfacesTrafficDeviceNameInterfaceName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceName', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsInterfacesTrafficDeviceNameInterfaceName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'interfaceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsInterfacesTrafficDeviceNameInterfaceName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdStatisticsInterfacesTopdelay - errors', () => {
      it('should have a postV2TenantTenantIdStatisticsInterfacesTopdelay function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdStatisticsInterfacesTopdelay === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsInterfacesTopdelay(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsInterfacesTopdelay', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdStatisticsChildtrafficBulk - errors', () => {
      it('should have a postV2TenantTenantIdStatisticsChildtrafficBulk function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdStatisticsChildtrafficBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsChildtrafficBulk(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsChildtrafficBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdStatisticsJnxcosBulk - errors', () => {
      it('should have a postV2TenantTenantIdStatisticsJnxcosBulk function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdStatisticsJnxcosBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsJnxcosBulk(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsJnxcosBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdStatisticsDelayFields - errors', () => {
      it('should have a postV2TenantTenantIdStatisticsDelayFields function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdStatisticsDelayFields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsDelayFields(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsDelayFields', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdStatisticsTeLspsBulk - errors', () => {
      it('should have a postV2TenantTenantIdStatisticsTeLspsBulk function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdStatisticsTeLspsBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsTeLspsBulk(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsTeLspsBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdStatisticsTeLspsFields - errors', () => {
      it('should have a postV2TenantTenantIdStatisticsTeLspsFields function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdStatisticsTeLspsFields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsTeLspsFields(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsTeLspsFields', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdStatisticsTeLspsTop - errors', () => {
      it('should have a postV2TenantTenantIdStatisticsTeLspsTop function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdStatisticsTeLspsTop === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsTeLspsTop(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsTeLspsTop', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdStatisticsTeLspsBulkdelay - errors', () => {
      it('should have a postV2TenantTenantIdStatisticsTeLspsBulkdelay function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdStatisticsTeLspsBulkdelay === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsTeLspsBulkdelay(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsTeLspsBulkdelay', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdStatisticsTeLspsDelayDeviceNameLspName - errors', () => {
      it('should have a postV2TenantTenantIdStatisticsTeLspsDelayDeviceNameLspName function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdStatisticsTeLspsDelayDeviceNameLspName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsTeLspsDelayDeviceNameLspName(null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsTeLspsDelayDeviceNameLspName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceName', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsTeLspsDelayDeviceNameLspName('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'deviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsTeLspsDelayDeviceNameLspName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lspName', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsTeLspsDelayDeviceNameLspName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'lspName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsTeLspsDelayDeviceNameLspName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdStatisticsTeLspsTraffic - errors', () => {
      it('should have a postV2TenantTenantIdStatisticsTeLspsTraffic function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdStatisticsTeLspsTraffic === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsTeLspsTraffic(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsTeLspsTraffic', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdStatisticsTeLspsTrafficDeviceName - errors', () => {
      it('should have a postV2TenantTenantIdStatisticsTeLspsTrafficDeviceName function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdStatisticsTeLspsTrafficDeviceName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsTeLspsTrafficDeviceName(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsTeLspsTrafficDeviceName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceName', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsTeLspsTrafficDeviceName('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'deviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsTeLspsTrafficDeviceName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdStatisticsTeLspsTrafficDeviceNameLspName - errors', () => {
      it('should have a postV2TenantTenantIdStatisticsTeLspsTrafficDeviceNameLspName function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdStatisticsTeLspsTrafficDeviceNameLspName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsTeLspsTrafficDeviceNameLspName(null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsTeLspsTrafficDeviceNameLspName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceName', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsTeLspsTrafficDeviceNameLspName('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'deviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsTeLspsTrafficDeviceNameLspName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lspName', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsTeLspsTrafficDeviceNameLspName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'lspName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsTeLspsTrafficDeviceNameLspName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdStatisticsTeLspsEventsBulk - errors', () => {
      it('should have a postV2TenantTenantIdStatisticsTeLspsEventsBulk function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdStatisticsTeLspsEventsBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsTeLspsEventsBulk(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsTeLspsEventsBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdStatisticsTeLspsEventsLspName - errors', () => {
      it('should have a postV2TenantTenantIdStatisticsTeLspsEventsLspName function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdStatisticsTeLspsEventsLspName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsTeLspsEventsLspName(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsTeLspsEventsLspName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lspName', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsTeLspsEventsLspName('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'lspName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsTeLspsEventsLspName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdStatisticsDeviceBulk - errors', () => {
      it('should have a postV2TenantTenantIdStatisticsDeviceBulk function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdStatisticsDeviceBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsDeviceBulk(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsDeviceBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdStatisticsDeviceTop - errors', () => {
      it('should have a postV2TenantTenantIdStatisticsDeviceTop function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdStatisticsDeviceTop === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsDeviceTop(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsDeviceTop', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdStatisticsLdpLspsBulk - errors', () => {
      it('should have a postV2TenantTenantIdStatisticsLdpLspsBulk function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdStatisticsLdpLspsBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsLdpLspsBulk(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsLdpLspsBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdStatisticsLdpLspsTrafficDeviceNameFec - errors', () => {
      it('should have a postV2TenantTenantIdStatisticsLdpLspsTrafficDeviceNameFec function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdStatisticsLdpLspsTrafficDeviceNameFec === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsLdpLspsTrafficDeviceNameFec(null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsLdpLspsTrafficDeviceNameFec', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceName', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsLdpLspsTrafficDeviceNameFec('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'deviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsLdpLspsTrafficDeviceNameFec', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fec', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsLdpLspsTrafficDeviceNameFec('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'fec is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsLdpLspsTrafficDeviceNameFec', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdStatisticsNetflowPrefixFlowDemand - errors', () => {
      it('should have a postV2TenantTenantIdStatisticsNetflowPrefixFlowDemand function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdStatisticsNetflowPrefixFlowDemand === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsNetflowPrefixFlowDemand(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsNetflowPrefixFlowDemand', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdStatisticsSidBulk - errors', () => {
      it('should have a postV2TenantTenantIdStatisticsSidBulk function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdStatisticsSidBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsSidBulk(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsSidBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdStatisticsSrTePolicyBulk - errors', () => {
      it('should have a postV2TenantTenantIdStatisticsSrTePolicyBulk function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdStatisticsSrTePolicyBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdStatisticsSrTePolicyBulk(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdStatisticsSrTePolicyBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdTransportControllers - errors', () => {
      it('should have a postV2TenantTenantIdTransportControllers function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdTransportControllers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdTransportControllers(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTransportControllers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV2TenantTenantIdTransportControllersTransportControllerIndex - errors', () => {
      it('should have a deleteV2TenantTenantIdTransportControllersTransportControllerIndex function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV2TenantTenantIdTransportControllersTransportControllerIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.deleteV2TenantTenantIdTransportControllersTransportControllerIndex(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTransportControllersTransportControllerIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transportControllerIndex', (done) => {
        try {
          a.deleteV2TenantTenantIdTransportControllersTransportControllerIndex('fakeparam', null, (data, error) => {
            try {
              const displayE = 'transportControllerIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTransportControllersTransportControllerIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV2TenantTenantIdTransportControllerGroups - errors', () => {
      it('should have a deleteV2TenantTenantIdTransportControllerGroups function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV2TenantTenantIdTransportControllerGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.deleteV2TenantTenantIdTransportControllerGroups(null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTransportControllerGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV2TenantTenantIdTransportControllerGroupsTransportControllerGroupName - errors', () => {
      it('should have a deleteV2TenantTenantIdTransportControllerGroupsTransportControllerGroupName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV2TenantTenantIdTransportControllerGroupsTransportControllerGroupName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.deleteV2TenantTenantIdTransportControllerGroupsTransportControllerGroupName(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTransportControllerGroupsTransportControllerGroupName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transportControllerGroupName', (done) => {
        try {
          a.deleteV2TenantTenantIdTransportControllerGroupsTransportControllerGroupName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'transportControllerGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTransportControllerGroupsTransportControllerGroupName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdHighAvailabilityHosts - errors', () => {
      it('should have a getV2TenantTenantIdHighAvailabilityHosts function', (done) => {
        try {
          assert.equal(true, typeof a.getV2TenantTenantIdHighAvailabilityHosts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getV2TenantTenantIdHighAvailabilityHosts(null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdHighAvailabilityHosts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdHighAvailabilityZkstatus - errors', () => {
      it('should have a getV2TenantTenantIdHighAvailabilityZkstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getV2TenantTenantIdHighAvailabilityZkstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getV2TenantTenantIdHighAvailabilityZkstatus(null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdHighAvailabilityZkstatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV2TenantTenantIdHighAvailabilityHighAvailability - errors', () => {
      it('should have a putV2TenantTenantIdHighAvailabilityHighAvailability function', (done) => {
        try {
          assert.equal(true, typeof a.putV2TenantTenantIdHighAvailabilityHighAvailability === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.putV2TenantTenantIdHighAvailabilityHighAvailability(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-putV2TenantTenantIdHighAvailabilityHighAvailability', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdHighAvailabilityStepdown - errors', () => {
      it('should have a postV2TenantTenantIdHighAvailabilityStepdown function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdHighAvailabilityStepdown === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdHighAvailabilityStepdown(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdHighAvailabilityStepdown', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdTopologyTopologyIdPathComputation - errors', () => {
      it('should have a postV2TenantTenantIdTopologyTopologyIdPathComputation function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdTopologyTopologyIdPathComputation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdPathComputation(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdPathComputation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdPathComputation('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdPathComputation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdTopologyTopologyIdMaintenances - errors', () => {
      it('should have a postV2TenantTenantIdTopologyTopologyIdMaintenances function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdTopologyTopologyIdMaintenances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdMaintenances(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdMaintenances', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.postV2TenantTenantIdTopologyTopologyIdMaintenances('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyTopologyIdMaintenances', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV2TenantTenantIdTopologyTopologyIdMaintenancesMaintenanceIndex - errors', () => {
      it('should have a deleteV2TenantTenantIdTopologyTopologyIdMaintenancesMaintenanceIndex function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV2TenantTenantIdTopologyTopologyIdMaintenancesMaintenanceIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdMaintenancesMaintenanceIndex(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdMaintenancesMaintenanceIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdMaintenancesMaintenanceIndex('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdMaintenancesMaintenanceIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing maintenanceIndex', (done) => {
        try {
          a.deleteV2TenantTenantIdTopologyTopologyIdMaintenancesMaintenanceIndex('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'maintenanceIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdTopologyTopologyIdMaintenancesMaintenanceIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV2TenantTenantIdTopologyRpcOptimize - errors', () => {
      it('should have a postV2TenantTenantIdTopologyRpcOptimize function', (done) => {
        try {
          assert.equal(true, typeof a.postV2TenantTenantIdTopologyRpcOptimize === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.postV2TenantTenantIdTopologyRpcOptimize(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-postV2TenantTenantIdTopologyRpcOptimize', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdTopologyRpcSimulation - errors', () => {
      it('should have a getV2TenantTenantIdTopologyRpcSimulation function', (done) => {
        try {
          assert.equal(true, typeof a.getV2TenantTenantIdTopologyRpcSimulation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyRpcSimulation(null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyRpcSimulation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdTopologyRpcSimulationUuid - errors', () => {
      it('should have a getV2TenantTenantIdTopologyRpcSimulationUuid function', (done) => {
        try {
          assert.equal(true, typeof a.getV2TenantTenantIdTopologyRpcSimulationUuid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyRpcSimulationUuid(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyRpcSimulationUuid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.getV2TenantTenantIdTopologyRpcSimulationUuid('fakeparam', null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyRpcSimulationUuid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdTopologyRpcSimulationUuidReportName - errors', () => {
      it('should have a getV2TenantTenantIdTopologyRpcSimulationUuidReportName function', (done) => {
        try {
          assert.equal(true, typeof a.getV2TenantTenantIdTopologyRpcSimulationUuidReportName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyRpcSimulationUuidReportName(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyRpcSimulationUuidReportName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.getV2TenantTenantIdTopologyRpcSimulationUuidReportName('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyRpcSimulationUuidReportName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reportName', (done) => {
        try {
          a.getV2TenantTenantIdTopologyRpcSimulationUuidReportName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'reportName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyRpcSimulationUuidReportName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdTopologyRpcDiverseTreeDesign - errors', () => {
      it('should have a getV2TenantTenantIdTopologyRpcDiverseTreeDesign function', (done) => {
        try {
          assert.equal(true, typeof a.getV2TenantTenantIdTopologyRpcDiverseTreeDesign === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyRpcDiverseTreeDesign(null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyRpcDiverseTreeDesign', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdTopologyRpcDiverseTreeDesignUuid - errors', () => {
      it('should have a getV2TenantTenantIdTopologyRpcDiverseTreeDesignUuid function', (done) => {
        try {
          assert.equal(true, typeof a.getV2TenantTenantIdTopologyRpcDiverseTreeDesignUuid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getV2TenantTenantIdTopologyRpcDiverseTreeDesignUuid(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyRpcDiverseTreeDesignUuid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.getV2TenantTenantIdTopologyRpcDiverseTreeDesignUuid('fakeparam', null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdTopologyRpcDiverseTreeDesignUuid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdHealth - errors', () => {
      it('should have a getV2TenantTenantIdHealth function', (done) => {
        try {
          assert.equal(true, typeof a.getV2TenantTenantIdHealth === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getV2TenantTenantIdHealth(null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdHealth', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdHealthNodes - errors', () => {
      it('should have a getV2TenantTenantIdHealthNodes function', (done) => {
        try {
          assert.equal(true, typeof a.getV2TenantTenantIdHealthNodes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getV2TenantTenantIdHealthNodes(null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdHealthNodes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdHealthNodesNodeId - errors', () => {
      it('should have a getV2TenantTenantIdHealthNodesNodeId function', (done) => {
        try {
          assert.equal(true, typeof a.getV2TenantTenantIdHealthNodesNodeId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getV2TenantTenantIdHealthNodesNodeId(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdHealthNodesNodeId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.getV2TenantTenantIdHealthNodesNodeId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdHealthNodesNodeId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdHealthNodesNodeIdTime - errors', () => {
      it('should have a getV2TenantTenantIdHealthNodesNodeIdTime function', (done) => {
        try {
          assert.equal(true, typeof a.getV2TenantTenantIdHealthNodesNodeIdTime === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getV2TenantTenantIdHealthNodesNodeIdTime(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdHealthNodesNodeIdTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.getV2TenantTenantIdHealthNodesNodeIdTime('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdHealthNodesNodeIdTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdHealthNodesNodeIdProcessName - errors', () => {
      it('should have a getV2TenantTenantIdHealthNodesNodeIdProcessName function', (done) => {
        try {
          assert.equal(true, typeof a.getV2TenantTenantIdHealthNodesNodeIdProcessName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getV2TenantTenantIdHealthNodesNodeIdProcessName(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdHealthNodesNodeIdProcessName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.getV2TenantTenantIdHealthNodesNodeIdProcessName('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdHealthNodesNodeIdProcessName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing processName', (done) => {
        try {
          a.getV2TenantTenantIdHealthNodesNodeIdProcessName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'processName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdHealthNodesNodeIdProcessName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdHealthNodesNodeIdProcessNameHistory - errors', () => {
      it('should have a getV2TenantTenantIdHealthNodesNodeIdProcessNameHistory function', (done) => {
        try {
          assert.equal(true, typeof a.getV2TenantTenantIdHealthNodesNodeIdProcessNameHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getV2TenantTenantIdHealthNodesNodeIdProcessNameHistory(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdHealthNodesNodeIdProcessNameHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.getV2TenantTenantIdHealthNodesNodeIdProcessNameHistory('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdHealthNodesNodeIdProcessNameHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing processName', (done) => {
        try {
          a.getV2TenantTenantIdHealthNodesNodeIdProcessNameHistory('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'processName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdHealthNodesNodeIdProcessNameHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV2TenantTenantIdHealthNodesThresholds - errors', () => {
      it('should have a getV2TenantTenantIdHealthNodesThresholds function', (done) => {
        try {
          assert.equal(true, typeof a.getV2TenantTenantIdHealthNodesThresholds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getV2TenantTenantIdHealthNodesThresholds(null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-getV2TenantTenantIdHealthNodesThresholds', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV2TenantTenantIdHealthNodesThresholdsDiskUtilization - errors', () => {
      it('should have a putV2TenantTenantIdHealthNodesThresholdsDiskUtilization function', (done) => {
        try {
          assert.equal(true, typeof a.putV2TenantTenantIdHealthNodesThresholdsDiskUtilization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.putV2TenantTenantIdHealthNodesThresholdsDiskUtilization(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-putV2TenantTenantIdHealthNodesThresholdsDiskUtilization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV2TenantTenantIdHealthNodesSmtpconfig - errors', () => {
      it('should have a putV2TenantTenantIdHealthNodesSmtpconfig function', (done) => {
        try {
          assert.equal(true, typeof a.putV2TenantTenantIdHealthNodesSmtpconfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.putV2TenantTenantIdHealthNodesSmtpconfig(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-putV2TenantTenantIdHealthNodesSmtpconfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing host', (done) => {
        try {
          a.putV2TenantTenantIdHealthNodesSmtpconfig('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'host is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-putV2TenantTenantIdHealthNodesSmtpconfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing port', (done) => {
        try {
          a.putV2TenantTenantIdHealthNodesSmtpconfig('fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'port is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-putV2TenantTenantIdHealthNodesSmtpconfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secure', (done) => {
        try {
          a.putV2TenantTenantIdHealthNodesSmtpconfig('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'secure is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-putV2TenantTenantIdHealthNodesSmtpconfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.putV2TenantTenantIdHealthNodesSmtpconfig('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-putV2TenantTenantIdHealthNodesSmtpconfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing password', (done) => {
        try {
          a.putV2TenantTenantIdHealthNodesSmtpconfig('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'password is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-putV2TenantTenantIdHealthNodesSmtpconfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enabled', (done) => {
        try {
          a.putV2TenantTenantIdHealthNodesSmtpconfig('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'enabled is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-putV2TenantTenantIdHealthNodesSmtpconfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV2TenantTenantIdHealthNodesSubscribers - errors', () => {
      it('should have a deleteV2TenantTenantIdHealthNodesSubscribers function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV2TenantTenantIdHealthNodesSubscribers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.deleteV2TenantTenantIdHealthNodesSubscribers(null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-juniper_northstar-adapter-deleteV2TenantTenantIdHealthNodesSubscribers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
