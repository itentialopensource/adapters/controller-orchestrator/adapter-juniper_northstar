
## 0.2.0 [05-21-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-juniper_northstar!1

---

## 0.1.1 [10-05-2021]

- Initial Commit

See commit 2313d3e

---
