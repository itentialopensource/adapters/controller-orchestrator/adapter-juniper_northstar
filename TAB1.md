# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Juniper-northstar System. The API that was used to build the adapter for Juniper-northstar is usually available in the report directory of this adapter. The adapter utilizes the Juniper-northstar API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Juniper NorthStar adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Juniper NorthStar. With this adapter you have the ability to perform operations on items such as:

- Topology
- Statistics
- Health

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
