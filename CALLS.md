## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Juniper NorthStar. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Juniper NorthStar.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Juniper NorthStar. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getV2TenantTenantIdTopology(tenantId, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV2TenantTenantIdTopologyTopologyId(tenantId, topologyId, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV2TenantTenantIdTopologyTopologyIdStatus(tenantId, topologyId, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV2TenantTenantIdTopologyTopologyIdStatusPce(tenantId, topologyId, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/status/pce?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV2TenantTenantIdTopologyTopologyIdStatusTopologyAcquisition(tenantId, topologyId, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/status/topologyAcquisition?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV2TenantTenantIdTopologyTopologyIdStatusPathComputationServer(tenantId, topologyId, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/status/pathComputationServer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV2TenantTenantIdTopologyTopologyIdStatusTransportTopologyAcquisition(tenantId, topologyId, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/status/transportTopologyAcquisition?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV2TenantTenantIdTopologyTopologyIdStatusAnalyticsCollection(tenantId, topologyId, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/status/analyticsCollection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV2TenantTenantIdTopologyTopologyIdStatusAnalyticsDatabase(tenantId, topologyId, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/status/analyticsDatabase?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdTopologyTopologyIdNodes(tenantId, topologyId, name, topologyIndex, topoObjectType, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV2TenantTenantIdTopologyTopologyIdNodesSearch(tenantId, topologyId, nameOptional, hostNameOptional, aSOptional, queryTypeOptional, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/nodes/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV2TenantTenantIdTopologyTopologyIdNodesNodeIndex(tenantId, topologyId, nodeIndex, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/nodes/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV2TenantTenantIdTopologyTopologyIdNodesNodeIndexHistory(tenantId, topologyId, nodeIndex, startOptional, endOptional, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/nodes/{pathv3}/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdTopologyTopologyIdLinks(tenantId, topologyId, endANode, endZNode, topologyIndex, topoObjectType, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/links?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV2TenantTenantIdTopologyTopologyIdLinksSearch(tenantId, topologyId, nameOptional, addressOptional, queryTypeOptional, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/links/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV2TenantTenantIdTopologyTopologyIdLinksLinkIndex(tenantId, topologyId, linkIndex, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/links/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV2TenantTenantIdTopologyTopologyIdLinksLinkIndexHistory(tenantId, topologyId, linkIndex, startOptional, endOptional, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/links/{pathv3}/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdTopologyTopologyIdTeLsps(tenantId, topologyId, name, fromAddress, toAddress, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/te-lsps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV2TenantTenantIdTopologyTopologyIdTeLspsSearch(tenantId, topologyId, nameOptional, fromOptional, operationalStatusOptional, queryTypeOptional, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/te-lsps/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV2TenantTenantIdTopologyTopologyIdTeLspsLspIndex(tenantId, topologyId, lspIndex, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/te-lsps/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV2TenantTenantIdTopologyTopologyIdTeLspsBulk(tenantId, topologyId, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/te-lsps/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV2TenantTenantIdTopologyTopologyIdTeLspsLspIndexHistory(tenantId, topologyId, lspIndex, startOptional, endOptional, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/te-lsps/{pathv3}/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdTopologyTopologyIdDemands(tenantId, topologyId, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/demands?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV2TenantTenantIdTopologyTopologyIdDemandsDemandIndex(tenantId, topologyId, demandIndex, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/demands/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV2TenantTenantIdTopologyTopologyIdDemandsBulk(tenantId, topologyId, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/demands/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV2TenantTenantIdNetconfProfiles(tenantId, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/netconf/profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdNetconfNetconfCollectionLiveNetwork(tenantId, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/netconf/netconfCollection/liveNetwork?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV2TenantTenantIdNetconfNetconfCollectionCollectionJobId(tenantId, collectionJobId, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/netconf/netconfCollection/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdTopologyTopologyIdTeContainers(tenantId, topologyId, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/te-containers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV2TenantTenantIdTopologyTopologyIdTeContainersContainerIndex(tenantId, topologyId, containerIndex, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/te-containers/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV2TenantTenantIdTopologyTopologyIdTeContainersBulk(tenantId, topologyId, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/te-containers/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdTopologyTopologyIdFacilities(tenantId, topologyId, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/facilities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV2TenantTenantIdTopologyTopologyIdFacilitiesFacilityIndex(tenantId, topologyId, facilityIndex, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/facilities/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdTopologyTopologyIdP2mp(tenantId, topologyId, name, fromAddress, plannedPropertiesBandwidth, plannedPropertiesSetupPriority, plannedPropertiesHoldingPriority, plannedPropertiesDesignAdminGroupsAttributeIncludeAny, plannedPropertiesDesignAdminGroupsAttributeIncludeAll, plannedPropertiesDesignAdminGroupsAttributeExclude, lsps, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/p2mp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndex(tenantId, topologyId, p2mpGroupIndex, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/p2mp/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV2TenantTenantIdTopologyTopologyIdP2mpBulk(tenantId, topologyId, p2mpGroupIndex, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/p2mp/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndexLspIndex(tenantId, topologyId, p2mpGroupIndex, lspIndex, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/p2mp/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV2TenantTenantIdTopologyTopologyIdP2mpP2mpGroupIndexBulk(tenantId, topologyId, p2mpGroupIndex, lspIndex, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/p2mp/{pathv3}/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV2TenantTenantIdTopologyTopologyIdRoutes(tenantId, topologyId, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/routes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV2TenantTenantIdTopologyTopologyIdRoutesNodeIndex(tenantId, topologyId, nodeIndex, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/routes/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdStatisticsDemandsBulk(tenantId, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/statistics/demands/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdStatisticsInterfacesDelayDeviceNameInterfaceName(tenantId, deviceName, interfaceName, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/statistics/interfaces/delay/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdStatisticsInterfacesBulk(tenantId, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/statistics/interfaces/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdStatisticsInterfacesFields(tenantId, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/statistics/interfaces/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdStatisticsInterfacesTop(tenantId, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/statistics/interfaces/top?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdStatisticsInterfacesChildtrafficDeviceNameInterfaceName(tenantId, deviceName, interfaceName, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/statistics/interfaces/childtraffic/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdStatisticsInterfacesBulkdelay(tenantId, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/statistics/interfaces/bulkdelay?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdStatisticsInterfacesTraffic(tenantId, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/statistics/interfaces/traffic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdStatisticsInterfacesTrafficDeviceName(tenantId, deviceName, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/statistics/interfaces/traffic/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdStatisticsInterfacesTrafficDeviceNameInterfaceName(tenantId, deviceName, interfaceName, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/statistics/interfaces/traffic/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdStatisticsInterfacesTopdelay(tenantId, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/statistics/interfaces/topdelay?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdStatisticsChildtrafficBulk(tenantId, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/statistics/childtraffic/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdStatisticsJnxcosBulk(tenantId, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/statistics/jnxcos/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdStatisticsDelayFields(tenantId, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/statistics/delay/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdStatisticsTeLspsBulk(tenantId, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/statistics/te-lsps/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdStatisticsTeLspsFields(tenantId, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/statistics/te-lsps/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdStatisticsTeLspsTop(tenantId, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/statistics/te-lsps/top?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdStatisticsTeLspsBulkdelay(tenantId, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/statistics/te-lsps/bulkdelay?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdStatisticsTeLspsDelayDeviceNameLspName(tenantId, deviceName, lspName, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/statistics/te-lsps/delay/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdStatisticsTeLspsTraffic(tenantId, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/statistics/te-lsps/traffic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdStatisticsTeLspsTrafficDeviceName(tenantId, deviceName, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/statistics/te-lsps/traffic/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdStatisticsTeLspsTrafficDeviceNameLspName(tenantId, deviceName, lspName, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/statistics/te-lsps/traffic/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdStatisticsTeLspsEventsBulk(tenantId, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/statistics/te-lsps/events/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdStatisticsTeLspsEventsLspName(tenantId, lspName, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/statistics/te-lsps/events/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdStatisticsDeviceBulk(tenantId, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/statistics/device/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdStatisticsDeviceTop(tenantId, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/statistics/device/top?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdStatisticsLdpLspsBulk(tenantId, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/statistics/ldp-lsps/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdStatisticsLdpLspsTrafficDeviceNameFec(tenantId, deviceName, fec, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/statistics/ldp-lsps/traffic/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdStatisticsNetflowPrefixFlowDemand(tenantId, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/statistics/netflow/prefix_flow_demand?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdStatisticsSidBulk(tenantId, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/statistics/sid/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdStatisticsSrTePolicyBulk(tenantId, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/statistics/sr-te-policy/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdTransportControllers(tenantId, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/transportControllers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV2TenantTenantIdTransportControllersTransportControllerIndex(tenantId, transportControllerIndex, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/transportControllers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV2TenantTenantIdTransportControllerGroups(tenantId, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/transportControllerGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV2TenantTenantIdTransportControllerGroupsTransportControllerGroupName(tenantId, transportControllerGroupName, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/transportControllerGroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV2TenantTenantIdHighAvailabilityHosts(tenantId, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/highAvailability/hosts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV2TenantTenantIdHighAvailabilityZkstatus(tenantId, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/highAvailability/zkstatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV2TenantTenantIdHighAvailabilityHighAvailability(tenantId, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/highAvailability/highAvailability?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdHighAvailabilityStepdown(tenantId, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/highAvailability/stepdown?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdTopologyTopologyIdPathComputation(tenantId, topologyId, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/pathComputation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdTopologyTopologyIdMaintenances(tenantId, topologyId, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/maintenances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV2TenantTenantIdTopologyTopologyIdMaintenancesMaintenanceIndex(tenantId, topologyId, maintenanceIndex, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/{pathv2}/maintenances/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV2TenantTenantIdTopologyRpcOptimize(tenantId, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/rpc/optimize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV2TenantTenantIdTopologyRpcSimulation(tenantId, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/rpc/simulation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV2TenantTenantIdTopologyRpcSimulationUuid(tenantId, uuid, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/rpc/simulation/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV2TenantTenantIdTopologyRpcSimulationUuidReportName(tenantId, uuid, reportName, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/rpc/simulation/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV2TenantTenantIdTopologyRpcDiverseTreeDesign(tenantId, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/rpc/diverseTreeDesign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV2TenantTenantIdTopologyRpcDiverseTreeDesignUuid(tenantId, uuid, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/topology/rpc/diverseTreeDesign/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV2TenantTenantIdHealth(tenantId, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/health?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV2TenantTenantIdHealthNodes(tenantId, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/health/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV2TenantTenantIdHealthNodesNodeId(tenantId, nodeId, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/health/nodes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV2TenantTenantIdHealthNodesNodeIdTime(tenantId, nodeId, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/health/nodes/{pathv2}/time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV2TenantTenantIdHealthNodesNodeIdProcessName(tenantId, nodeId, processName, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/health/nodes/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV2TenantTenantIdHealthNodesNodeIdProcessNameHistory(tenantId, nodeId, processName, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/health/nodes/{pathv2}/{pathv3}/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV2TenantTenantIdHealthNodesThresholds(tenantId, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/health/nodes/thresholds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV2TenantTenantIdHealthNodesThresholdsDiskUtilization(tenantId, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/health/nodes/thresholds/diskUtilization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV2TenantTenantIdHealthNodesSmtpconfig(tenantId, host, port, secure, username, password, enabled, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/health/nodes/smtpconfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV2TenantTenantIdHealthNodesSubscribers(tenantId, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant/{pathv1}/health/nodes/subscribers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
