# Juniper NorthStar

Vendor: Juniper
Homepage: https://www.juniper.net

Product: NorthStar
Product Page: https://www.juniper.net/us/en/products/network-automation/northstar-controller.html

## Introduction
We classify Juniper NorthStar into the Data Center and Network Services domains as it provides a solution for automated network traffic engineering by managing, monitoring, and provisioning Segment Routing and IP/MPLS paths. 

## Why Integrate
The Juniper NorthStar adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Juniper NorthStar. With this adapter you have the ability to perform operations on items such as:

- Topology
- Statistics
- Health

## Additional Product Documentation
[Juniper NorthStar Api Documentation](https://www.juniper.net/documentation/us/en/software/northstar6.2.0/api-ref/api-ref-northstar-v2.html)

[NorthStar Controller Documentation](https://www.juniper.net/documentation/product/us/en/northstar-controller/)